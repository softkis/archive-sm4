       SELECT books
           ASSIGN       TO DISK "books.dat"
           ORGANIZATION IS INDEXED
           ACCESS MODE  IS DYNAMIC
           FILE STATUS  IS book-status
           RECORD KEY   IS book-key
           ALTERNATE RECORD KEY IS author-key = book-author, book-title.
