      *{Bench}prg-comment
      * Program1.cbl
      * Program1.cbl is generated from C:\SM4\Program1.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. Program1.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 15:36:34.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
           COPY "USER.FC".
           COPY "MENU.FC".
           COPY "PAYS.FC".


       DATA                        DIVISION.

       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
           COPY "USER.FDE".
           COPY "MENU.FDE".
           COPY "PAYS.FDE".


       WORKING-STORAGE             SECTION.

      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

acu
       77  char-win-handle
                  USAGE IS HANDLE OF WINDOW.
       77  char-font-handle
                  USAGE IS HANDLE OF FONT.
       copy "fonts.def".
acu

           COPY "V-VAR.CPY".
           COPY "FUSER.REC".
           COPY "PARMOD.REC".
           COPY "MESSAGE.REC".
      *     COPY "PAYS.REC".
           COPY "POCL.REC".

       01  I_text PIC x(1024).
       01  O_text PIC x(1024).
       01  cnt pic 9999.
       01  eof pic 9.

           

      *{Bench}copy-working
       COPY "Program1.wrk".
      *{Bench}end
      * LINKAGE                     SECTION.
           COPY "V-LINK.CPY".
           COPY "MENU.LNK".
           COPY "PAYS.LNK".
           COPY "USER.LNK".
      *{Bench}linkage
      *{Bench}end
       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "Program1.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end


       DECLARATIVES.

      * FILE-ERR-PROC SECTION.
      * 
      *     USE AFTER ERROR PROCEDURE ON USER.
      * 
      * FILE-ERROR-PROC.
      *
      *     IF FS-USER  = "35"
      *        CALL "1-USER" USING LINK-V 
      *     ELSE
      *        CALL "C$RERR" USING EXTENDED-STATUS
      *        CALL "0-ERROR" USING LINK-V
      *        STOP RUN
      *     END-IF.
      *
       END DECLARATIVES.


             perform leseMenu.


       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-Screen1-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .


       leseRw.
           READ MENU previous NO LOCK   
           END-READ
           move MN-DESCRIPTION to Screen1-Ef-1-value
           Display Screen1-Ef-1
           .

       leseVw.
            READ MENU next NO LOCK   
           END-READ
           move MN-DESCRIPTION to Screen1-Ef-1-value
           Display Screen1-Ef-1
           .

       leseSatz.
            READ MENU next NO LOCK   
             at end
              move 1 to eof
              not at end 
                 compute cnt = cnt + 1
           END-READ
           .

       leseMenu.
           INITIALIZE MN-RECORD.
           INITIALIZE US-KEY.
           INITIALIZE FS-USER.
           INITIALIZE FS-PAYS.
                      
           INITIALIZE I_text  .
           INITIALIZE O_text  .
           INITIALIZE cnt  .

           move 0 to cnt.
           move 0 to eof.

           OPEN INPUT MENU   .
           
           INITIALIZE MN-BODY.

 
      *    CLOSE MENU

      *     Display Screen1-Ef-1
           .

       leseDatei.

           move "hallo karl wie gehts" to Screen1-Ef-1-Value
           Display Screen1-Ef-1

           perform leseMenu

           .


      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "Program1.prd".
       COPY "Program1.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
