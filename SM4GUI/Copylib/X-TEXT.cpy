      
           01 x-text.
             05  x-text-mode pic 99 value 0.
      
             05  fs pic xx.
             05  fnam pic x(256) value "0003AAAA.D".
             
             05  flang pic xx.
      
             05  fnamcpy pic x(256) value all space.
             05  foutpath pic x(256) value all space.
      
      *     *01  fnamt pic x(256).
      
             05  fcont.
                 10 fline pic x(100) occurs 99.
             05  fcnt pic 99.
      
             05  h-control occurs 99.
                 10 h-control-nam pic x(50).
                 10 h-control-para pic x(6).
                 10 filler redefines h-control-para.
                     15 h-row pic 99.
                     15 h-col pic 99.
                     15 h-len pic 99.
      
             05  h-cnt   pic 999.
      
             05  h-idx   pic 99.
             05  h-tmp   pic x(100).
             05  h-str   pic x(100).
      
             05  h-r pic 99.
             05  h-c pic 99.
             05  h-l pic 99.
             05  h-fl pic 999.
      
