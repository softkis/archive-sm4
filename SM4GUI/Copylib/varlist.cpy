
       01  var-call.

           78  var-call-max-tabs value 10.
           78  var-call-max-line value 100.

           02  var-callnam  pic x(10).
           
           02  var-title pic x(40).

           02  var-line  pic x(var-call-max-line).

           02  var-tabs.
                 05  var-data pic 99 occurs 10.
                 05  var-disp pic 99 occurs 10.
                 05  var-caption pic x(30) occurs var-call-max-tabs.
                 05  var-tabs-cnt pic 99.

           02 var-call-result pic 99.
           02 var-call-return pic x(var-call-max-line).

           02 var-call-return-R redefines var-call-return.
              03 var-call-matricule   pic 9(11).
              03 var-call-nom         pic x(40).
              03 var-call-localite    pic x(40).
      *     02 var-call-return-FR redefines var-call-return.
      *        03 var-call-fr-key      pic 9(8).
      *        03 var-call-fr-nom      pic x(40).
      *        03 var-call-fr-localite pic x(50).
           02 var-call-return-REG redefines var-call-return.
              03 var-call-REG-PERSON      PIC 9(8).
              03 var-call-REG-NOM         PIC X(40).  
              03 var-call-REG-DATE-NAISS  PIC X(10).
              03 var-call-REG-DATE-ANCIEN PIC X(10).
              03 var-call-REG-DATE-DEPART PIC X(10).
              03 filler                   PIC X(10).  
              03 var-call-REG-FIRME       PIC 9(6).

           02  var-para.
                 05  var-par pic x(30) occurs 20.
                 05  var-para-cnt pic 99.


