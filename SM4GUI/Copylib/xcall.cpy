
       01  x-call.

           78 x-call-max-tabs      value 10.
           78 x-call-max-line      value 100.

           02 x-call-mode pic 9.
           02 x-call-status pic 99.

           02 x-call-result pic 99.
           02 x-call-return pic x(x-call-max-line).

           02 x-call-tabs.
                 05  x-data pic 99 occurs x-call-max-tabs.
                 05  x-disp pic 99 occurs x-call-max-tabs.
                 05  x-caption pic x(30) occurs x-call-max-tabs.
                 05  x-tabs-cnt pic 99.

           02  x-call-para.
                 05  x-par pic x(30) occurs 20.
                 05  x-para-cnt pic 99.

           78 x-call-open      value 0.
           78 x-call-first     value 1.
           78 x-call-next      value 2.
           78 x-call-close     value 3.
           78 x-call-gettabs   value 4.
           78 x-call-setparas  value 5.


