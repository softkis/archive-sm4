       SELECT OPTIONAL pays
           ASSIGN       TO DISK "S-PAYS.dta"
           ORGANIZATION IS INDEXED
           ACCESS MODE  IS DYNAMIC
           LOCK MODE    IS MANUAL
           FILE STATUS  IS FS-PAYS
           RECORD KEY   IS PAYS-KEY
           ALTERNATE RECORD KEY IS PAYS-KEY-1.
