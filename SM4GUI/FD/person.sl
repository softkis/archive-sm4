       SELECT OPTIONAL person
           ASSIGN       TO DISK "a-person.dta"
           ORGANIZATION IS INDEXED
           ACCESS MODE  IS DYNAMIC
           LOCK MODE    IS MANUAL
           FILE STATUS  IS fs-pers
           RECORD KEY   IS PR-MATRICULE
           ALTERNATE RECORD KEY IS pr-ak1 = PR-MATCHCODE, PR-MATRICULE.
