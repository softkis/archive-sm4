       SELECT OPTIONAL land
           ASSIGN       TO DISK "land.dta"
           ORGANIZATION IS INDEXED
           ACCESS MODE  IS DYNAMIC
           FILE STATUS  IS land-status
           RECORD KEY   IS land-number
           ALTERNATE RECORD KEY IS land-string
           WITH DUPLICATES .
