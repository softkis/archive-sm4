      *{Bench}prg-comment
      * 1-FAMIL.cbl
      * 1-FAMIL.cbl is generated from C:\SM4\SM4GUI\1-FAMIL.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-FAMIL.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 15:36:44.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "fonts.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end


           COPY "X-TEXT.cpy"  .


      *{Bench}copy-working
       COPY "1-FAMIL.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
      *{Bench}end


           COPY  "V-LINK.CPY".
           COPY  "V-ERR.CPY".

       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-FAMIL.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-FAMIL-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .


           copy "0220NNNN-cpy.cbl.txt"   .


      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-FAMIL.prd".
       COPY "1-FAMIL.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
