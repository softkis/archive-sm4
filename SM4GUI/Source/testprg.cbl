      *{Bench}prg-comment
      * testprg.cbl
      * testprg.cbl is generated from C:\SM4\SM4GUI\testprg.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. testprg.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Samstag, 6. Juni 2009 13:40:52.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
           COPY "MENU.FC".

      DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
           COPY "MENU.FDE".

       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".
           COPY "MENU.LNK".



      *{Bench}copy-working
       COPY "testprg.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
      *{Bench}end
           COPY "V-LINK.CPY".
           COPY "V-ERR.CPY".      

       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "testprg.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION .
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-Screen1-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

       leseDatei.

           move "hallo karl" To Screen1-Ef-1-Value
           Display Screen1-Ef-1

           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "testprg.prd".
       COPY "testprg.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
