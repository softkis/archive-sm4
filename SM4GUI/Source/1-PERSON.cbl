      *{Bench}prg-comment
      * 1-PERSON.cbl
      * 1-PERSON.cbl is generated from C:\SM4\SM4GUI\1-PERSON.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-PERSON.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 15:36:36.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      * print sl
       SELECT PRINTF
              ASSIGN TO PRINT PTR-DEV-NAME
              FILE   STATUS   IS STAT-PRINTF.
      *{Bench}end

           COPY "PERSON.FC".

       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      * print fd
       FD PRINTF    LABEL   RECORD  OMITTED.
       01 PRINTF-R.
          05 PRINTF-01              PIC X OCCURS 1024 TIMES.
      *{Bench}end

           COPY "PERSON.FDE".

       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "fonts.def".
       COPY "acureport.def".
       COPY "showmsg.def".
      *{Bench}end

           copy "X-TEXT.cpy"  .
           copy "varlist.cpy" .
      *     COPY  "V-ERR.CPY".
      *     COPY  "FIRME.LNK".
      *     COPY  "PERSON.LNK".

           01 pers1-first-run pic 9 value 0.
           01 title-0200aaaa pic x(30) value "tab1".
           01 title-0201aaaa pic x(30) value "tab2".

           01 1-person-first   pic 9.
           01 1-person-last    pic 9.


      *{Bench}copy-working
       COPY "1-PERSON.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
       COPY "1-PERSON.lks".
      *{Bench}end
           SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-PERSON.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION USING LINK-V, LINK-RECORD.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end


       move 1 to 1-person-first.
       move 0 to 1-person-last.
       perform openfile.


       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-PERS1-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

           perform closefile.

       copy "0200NNNN-cpy.cbl.txt"   .
       copy "0201NNNN-cpy.cbl.txt"   .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-PERSON.prd".
       COPY "1-PERSON.evt".
       COPY "1-PERSON.rpt".
      *{Bench}end


       openfile.
           open I-O personne 
           INITIALIZE PR-RECORD
           .

       closefile.
           close personne
           .



       REPORT-COMPOSER SECTION.
      *{Bench}PERSRPT-masterprintpara
       Acu-RPT-PERSRPT-MASTER-PRINT-LOOP.
      *Before do print paragraph
           PERFORM PERSRPT-BeforeDoPrint
           PERFORM UNTIL PERSRPT-DOPRINTRTN-LOOP = 0
                   PERFORM ACU-RPT-PERSRPT-DO-PRINT-RTN
      *After do print paragraph
                   PERFORM PERSRPT-AfterDoPrint
           END-PERFORM
           .
      *{Bench}end
            
