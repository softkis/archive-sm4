       IDENTIFICATION DIVISION.
       PROGRAM-ID.                      1-REGIST-X.
       INSTALLATION.                    comment-entry.
       DATE-WRITTEN.                    2008/XX/XX - 99:99:99.
       DATE-COMPILED.                   2008/XX/XX - 00:00:00.

      ******************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.                 computer-name.
       OBJECT-COMPUTER.                 computer-name.
      *SPECIAL-NAMES.
      *   CURSOR     IS  cursor-name,
      *   CRT STATUS IS  crt-status-name.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

           COPY "REGISTRE.FC".
           COPY "PERSON.FC".

      *     SELECT [OPTIONAL] file-name
      *            ASSIGN TO [device] [file-spec]
      *            FILE STATUS IS status-variable.
      *I-O-CONTROL.
      *      i-o-control-entry

      *****************************************************************
       DATA DIVISION.
       FILE SECTION.

           COPY "REGISTRE.FDE".
           COPY "PERSON.FDE".

      * FD  file-name [IS EXTERNAL] [IS GLOBAL]
      *   [ file-desc { record-description } ...] ... ]
      * SD  file-name
      *   [ sort-desc { record-description } ...]

       WORKING-STORAGE SECTION.
      *For C$CALLERR routine to get why the CALL statement Failed.
       77  ERR-CODE                      pic x(2).
       77  ERR-MESSAGE                   pic x(80).
      *For C$RERR routine to get the extendeed file status information.
       01  EXTEND-STAT.
           03 primary-error              pic x(2).
           03 secondary-error            pic x(10).
       01  TEXT-MESSAGE                  pic x(40).
      *For C$RERRNAME routine to get the last file used in an I/O statement.
       77  LAST-FILENAME                 PIC X(40).


       01  H-REGIST-RECORD.
              03 H-REGIST-PERSON           PIC 9(8).
              03 H-REGIST-NOM              PIC X(40).
              03 H-REGIST-DATE-NAISS       PIC X(10).
              03 H-REGIST-DATE-ANCIEN      PIC X(10).
              03 H-REGIST-DATE-DEPART      PIC X(10).
              03 FILLER                    PIC X(10). 
              03 H-REGIST-FIRME            PIC 9(6).

      * 01  H-REGIST-LINK.
      *        03 H-REGIST-FIRME-LINK            PIC 9(6).
      *        03 H-REGIST-PERSON-LINK           PIC 9(8).


       01 HE-REG.
           02 H-R PIC Z(4) OCCURS 20.

           02 HE-TEMP.
              03 HE-JJ    PIC 99.
              03 FILLER   PIC X VALUE ".".
              03 HE-MM    PIC 99.
              03 FILLER   PIC X VALUE ".".
              03 HE-AA    PIC 9999.

           COPY "V-VAR.CPY".
           COPY "PERSON.LK".
           COPY "PERSON.LNK".

       01  CRIT-FLAG             PIC 9 VALUE 0.
       01  CRITERES-1            PIC X(50).
       01  HLP-CRIT REDEFINES CRITERES-1.
           03 CRIT               PIC X OCCURS 50.

       01  TST                  PIC X(4).

       01  ITEMS.
           02 ITEM-A OCCURS 10.
              03 ITEM-IDX        PIC 99.
              03 ITEM            PIC X(10).
              03 ITEM-A REDEFINES ITEM.
                 04 ITEM-2       PIC X(2).
                 04 ITEM-F2      PIC X(8).
              03 ITEM-B REDEFINES ITEM.
                 04 ITEM-3       PIC X(3).
                 04 ITEM-F3      PIC X(7).
              03 ITEM-C REDEFINES ITEM.
                 04 ITEM-4       PIC X(4).
                 04 ITEM-F4      PIC X(6).
              03 ITEM-D REDEFINES ITEM.
                 04 ITEM-5       PIC X(5).
                 04 ITEM-F5      PIC X(5).
              03 ITEM-E REDEFINES ITEM.
                 04 ITEM-6       PIC X(6).
                 04 ITEM-F6      PIC X(4).
              03 ITEM-F REDEFINES ITEM.
                 04 ITEM-7       PIC X(7).
                 04 ITEM-F7      PIC X(3).
              03 ITEM-G REDEFINES ITEM.
                 04 ITEM-8       PIC X(8).
                 04 ITEM-F8      PIC X(2).
              03 ITEM-H REDEFINES ITEM.
                 04 ITEM-9       PIC X(9).
                 04 ITEM-F9      PIC X(1).

       01  CHOIX                 PIC 9(4).
       01  CHOIX-A               PIC X(50).
       01  CHOIX-A1 REDEFINES CHOIX-A.
           02 CHOIX-1            PIC 9.
           02 CHOIX-F1           PIC X(9).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A2 REDEFINES CHOIX-A.
           02 CHOIX-2            PIC 9(2).
           02 CHOIX-F2           PIC X(8).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A3 REDEFINES CHOIX-A.
           02 CHOIX-3            PIC 9(3).
           02 CHOIX-F3           PIC X(7).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A4 REDEFINES CHOIX-A.
           02 CHOIX-4            PIC 9(4).
           02 CHOIX-F4           PIC X(6).
           02 CHOIX-FX           PIC X(40).

       01  COMPTEUR              PIC 99.
       01  PRECISION             PIC 9.


       LINKAGE SECTION.
      * [ Data Desciption Entry ....]

           copy "V-LINK.cpy"
           copy "xcall.cpy"   

       SCREEN SECTION.
      * [ Screen Description Entry ... ]

      ******************************************************************
       PROCEDURE DIVISION using LINK-V X-CALL.
      *        [ {USING | CHAINING} {parameter} ... ] .

           evaluate x-call-mode
              when x-call-open
                  move 66 to EXC-KEY
                  move "A" to A-N
                  move 0 to x-call-status
                  initialize link-matchcode

              when x-call-first
                  perform nextitem

              when x-call-next
                  perform nextitem

              when x-call-close
                   continue
                 
              when x-call-gettabs
                 move 8 to x-data(1)
                 move 5 to x-disp(1)
      
                 move 40  to x-data(2)
                 move 30 to x-disp(2)
      
                 move 10 to x-data(3)
                 move 10 to x-disp(3)
      
                 move 10 to x-data(4)
                 move 10 to x-disp(4)
      
                 move 10 to x-data(5)
                 move 10 to x-disp(5)

                 move 5 to x-tabs-cnt

                 move "01" to x-caption( 1 )
                 move "02" to x-caption( 2 )
                 move "03" to x-caption( 3 )
                 move "04" to x-caption( 4 )
                 move "05" to x-caption( 5 )
                   
               when x-call-setparas

                 move x-para-cnt to x-para-cnt

                 continue
                 
           end-evaluate

           Exit Program.
      *    Stop Run.

       nextitem.

           initialize x-call-para

              if x-para-cnt = 0 
                 perform next-regist
      *           perform next-person
              else
      *           move x-par( 1 ) to link-matchcode
      *           perform Read-Fr
              end-if

              if FS-REGISTRE = "00" 
                 move REG-FIRME        to H-REGIST-FIRME
                 move REG-PERSON       to H-REGIST-PERSON

                 move REG-ANCIEN-J     to HE-JJ
                 move REG-ANCIEN-M     to HE-MM
                 move REG-ANCIEN-A     to HE-AA
                 move HE-TEMP          to H-REGIST-DATE-ANCIEN
                 initialize HE-TEMP


                 perform GET-PERS

                 string PR-NOM DELIMITED BY "  "
                        " " 
                        PR-PRENOM DELIMITED BY SIZE 
                        into H-REGIST-NOM
                 end-string

                 move PR-NAISS-J      to HE-JJ
                 move PR-NAISS-M      to HE-MM
                 move PR-NAISS-A      to HE-AA
                 move HE-TEMP         to H-REGIST-DATE-NAISS
                 initialize HE-TEMP

                 move H-REGIST-RECORD   to x-call-return

              end-if

      *        move FS-REGISTRE to x-call-status                
              move FS-PERSON   to x-call-status                
           .

       NEXT-REGIST.
           MOVE "N" TO LNK-A-N.
      *     EVALUATE EXC-KEY
      *     WHEN  2 THRU 3
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   END-IF
                   MOVE 2 TO LNK-PRESENCE
           
           CALL "6-REGIS" USING LINK-V REG-RECORD A-N EXC-KEY.


       NEXT-PERSON.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 3
              MOVE "A" TO A-N.
           MOVE PR-RECORD TO LINK-RECORD.
           CALL "6-PERSON" USING LINK-V LINK-RECORD A-N EXC-KEY.
           MOVE LINK-RECORD TO PR-RECORD.
           .

       GET-PERS.
           MOVE REG-MATRICULE TO PR-MATRICULE.
           CALL "6-PERSON" USING LINK-V PR-RECORD A-N FAKE-KEY.

      * DECLARATIVES.
      * section-name SECTION [ segment-no].
      *    declarative-sentence
      * paragraph-name.
      *     sentence ...   ...
      * END DECLARATIVES.
      
      * Section-name SECTION [ segment-no].
      *    declarative-sentence
      * Paragraph-name.
      *     sentence ...   ...
       Main Section.

           Exit Program.
           Stop Run.

       Library-Routines Section.
      * Library Routines Description

       Display-Call-Err-Message.
           CALL "C$CALLERR" USING ERR-CODE, ERR-MESSAGE
           DISPLAY MESSAGE BOX
                   ERR-MESSAGE,
                   TITLE IS "Call Statement Failed".
                   
       Display-Extended-File-Status.
           CALL "C$RERR" USING EXTEND-STAT, TEXT-MESSAGE.
           CALL "C$RERRNAME" USING LAST-FILENAME.
           MOVE SPACES TO ERR-MESSAGE.
           String "File Name :" delimited by size
                  LAST-FILENAME delimited by spaces
                  " Status ["   delimited by size
                  primary-error delimited by size
                  ","           delimited by size
                  secondary-error delimited by spaces
                  "] "          delimited by size
                  TEXT-MESSAGE  delimited by size
              into ERR-MESSAGE
           End-String.
           DISPLAY MESSAGE BOX
                   ERR-MESSAGE,
                   TITLE IS "File Status".
                   
       Display-File-Status-Codes.
           Evaluate ERR-CODE
               When "00"
                  Display Message "Operation successful."
               When "10"
                  Display Message "End of file."
               When "22"
                  Display Message "Duplicate key found but not allowed."
               When "23"
                  Display Message "Record not found."
               When "34"
                  Display Message "Disk full for sequential file or sort
      -                             " file."
               When "35"
                  Display Message "File not found."
               When "41"
                  Display Message "File is already open."
               When "42"
                  Display Message "File not open."
               When "30"
               When "39"
               When other
                  Perform Display-Extended-File-Status
           End-Evaluate.
