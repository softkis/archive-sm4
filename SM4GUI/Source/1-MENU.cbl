      *{Bench}prg-comment
      * 1-MENU.cbl
      * 1-MENU.cbl is generated from C:\SM4\SM4GUI\1-MENU.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-MENU.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 15:36:38.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
           COPY "MENU.FC".
       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
           COPY "MENU.FDE".
       WORKING-STORAGE             SECTION.
           COPY "V-VAR.CPY".
           COPY "V-LINK.CPY".
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

      *{Bench}copy-working
       COPY "1-MENU.wrk".
      *{Bench}end

sm3
       01  H-M.
           02 HELP-MENU OCCURS 15.
              03 HELP-KEY.
                 04 HELP-LEVEL.
                    05 HELP-LEV          PIC 99 OCCURS 5.

              03 HELP-BODY.
                 04 HELP-PROG-NAME       PIC X(8).
                 04 HELP-PROG-NAME-R REDEFINES HELP-PROG-NAME.
                    05 HELP-PROG-BEG     PIC XX.
                    05 HELP-PROG-END     PIC X(6).
                 04 HELP-COMPETENCE      PIC 9.
                 04 HELP-CALL            PIC X(8).
                 04 HELP-PASSWORD        PIC X(22).
                 04 HELP-DESCRIPTION     PIC X(40).
                 04 HELP-TEXT            PIC 9(8).
                 04 HELP-PROG-NUMBER     PIC 9(8).
                 04 HELP-EXTENSION-1     PIC X(8).
                 04 HELP-EXTENSION-2     PIC X(8).
                 04 HELP-TEST            PIC 9.
                 04 HELP-BATCH           PIC 9.
                 04 HELP-LOG             PIC 9.
                 04 HELP-FILLER          PIC X(20).
sm3

           01 h-menu-first pic 9.
           01 h-menu-last pic 9.



       LINKAGE                     SECTION.
      *{Bench}linkage
      *{Bench}end
       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-MENU.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

           move 1 to h-menu-first     .
           move 0 to  h-menu-last    .


       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-Screen1-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn


           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-MENU.prd".
       COPY "1-MENU.evt".
      *{Bench}end


       openfile.
           open I-O MENU 
           INITIALIZE MN-record
  
           .

       closefile.
           close MENU
           .




       REPORT-COMPOSER SECTION.
