      *{Bench}prg-comment
      * 1-FICHE.cbl
      * 1-FICHE.cbl is generated from C:\SM4\SM4GUI\1-FICHE.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-FICHE.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 15:36:42.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end

           COPY "FICHE.FC".

       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end

           COPY "FICHE.FDE".

       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "fonts.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end
           
      
           01 fiche-first-run pic 9 value 0.
           01 title-s1 pic x(30) value "tab1".
           01 title-s2 pic x(30) value "tab2".

           copy "X-TEXT.cpy"  .

      *{Bench}copy-working
       COPY "1-FICHE.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
      *{Bench}end

           COPY  "V-LINK.CPY".

       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-FICHE.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-FICHE-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

           copy "0231NNNN-cpy.cbl.txt"   .

           .
      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-FICHE.prd".
       COPY "1-FICHE.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
