      *{Bench}prg-comment
      * login.cbl
      * login.cbl is generated from C:\SM4\SM4GUI\login.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. login.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Montag, 1. Juni 2009 09:57:15.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
           COPY "USER.FC".
       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
           COPY "USER.FDE".

       WORKING-STORAGE             SECTION.
       COPY "V-VAR.CPY".
       COPY "PARMOD.REC".



       01  X-PASSWORD            PIC X(22).
       01  XX-PASSWORD           PIC X(22).



      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end

      *{Bench}copy-working
       COPY "login.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
           COPY "V-LINK.CPY".

      *{Bench}linkage
      *{Bench}end
       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "login.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-logg-Routine
      *{Bench}end


           perform closefile_USER
           

           PERFORM Acu-Exit-Rtn

           
           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "login.prd".
       COPY "login.evt".
      *{Bench}end



       openfile_USER.
           OPEN INPUT USER
           INITIALIZE US-REC-DET.



       closefile_USER.
           close USER
      *    STOP RUN.
           .





       REPORT-COMPOSER SECTION.


