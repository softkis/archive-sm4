       IDENTIFICATION DIVISION. 
       PROGRAM-ID. PagedGrid. 
       AUTHOR. Bob Cavanagh. 
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
       SELECT Samplegrid 
           ASSIGN       TO DISK "samplegrid.dat" 
           ORGANIZATION IS INDEXED 
           ACCESS MODE  IS DYNAMIC 
           FILE STATUS  IS samplegrid-status 
           RECORD KEY   IS samplegridkey 
           ALTERNATE RECORD KEY IS samplealtkey = last-name, first-name. 
       
       DATA DIVISION. 
       FILE SECTION. 
       FD  Samplegrid. 
       01  samplegrid-record. 
           05 samplegridkey. 
               10 first-name          PIC  X(20). 
               10 last-name           PIC  X(20). 
           05 samplegrid-extension    PIC  X(4). 
           05 samplegrid-department   PIC  X(15). 
           05 manager-flag            PIC  9. 
           05 samplegrid-email        PIC  X(15). 
           05 samplegrid-home-phone   PIC  X(15). 
      * 
       WORKING-STORAGE SECTION. 
       78  EVENT-ACTION-FAIL          VALUE 4. 
       78  MSG-CLOSE                  VALUE 16415. 
       78  MSG-PAGED-NEXT             VALUE 16419. 
       78  MSG-PAGED-PREV             VALUE 16420. 
       78  MSG-PAGED-FIRST            VALUE 16423. 
       78  MSG-PAGED-LAST             VALUE 16424. 
       78  ACTION-FIRST-PAGE          VALUE 10. 
      * 
       01  EVENT-STATUS 
           IS SPECIAL-NAMES EVENT STATUS. 
         03  EVENT-TYPE               PIC X(4) COMP-X. 
         03  EVENT-WINDOW-HANDLE      HANDLE OF WINDOW. 
         03  EVENT-CONTROL-HANDLE     HANDLE. 
         03  EVENT-CONTROL-ID         PIC XX COMP-X. 
         03  EVENT-DATA-1             SIGNED-SHORT. 
         03  EVENT-DATA-2             SIGNED-LONG. 
         03  EVENT-ACTION             PIC X COMP-X. 
      * 
       01  SCREEN-CONTROL 
           IS SPECIAL-NAMES SCREEN CONTROL. 
         03  ACCEPT-CONTROL      PIC 9. 
         03  CONTROL-VALUE       PIC 999. 
         03  CONTROL-HANDLE      HANDLE. 
         03  CONTROL-ID          PIC XX COMP-X. 
      * 
       77 Key-Status IS SPECIAL-NAMES CRT STATUS PIC 9(4) VALUE 0. 
          88 Exit-Pushed VALUE 27. 
          88 Message-Received VALUE 95. 
          88 Event-Occurred VALUE 96. 
          88 Screen-No-Input-Field VALUE 97. 
      * 
       77 samplegrid-status PIC X(2). 
         88 Valid-Samplegrid VALUE "00" THRU "09". 
       
       77 Form1-Handle HANDLE OF WINDOW. 
       77 Arial24B HANDLE OF FONT. 
      * 
       01  GRID-COLUMN-HEADINGS. 
          05  FILLER PIC X(20) VALUE "FIRST NAME". 
          05  FILLER PIC X(20) VALUE "LAST NAME". 
          05  FILLER PIC X(4)  VALUE "EXT". 
          05  FILLER PIC X(15) VALUE "DEPT.". 
          05  FILLER PIC X(15) VALUE "E-MAIL". 
          05  FILLER PIC X(15) VALUE "HOME PHONE".         
      * 
       01  GRID-DATA. 
         03 GRID-KEY. 
           05  GRID-FIRST-NAME PIC X(20). 
           05  GRID-LAST-NAME  PIC X(20). 
         03  GRID-EXTENSION    PIC X(4). 
         03  GRID-DEPARTMENT   PIC X(15).     
         03  GRID-EMAIL        PIC X(15). 
         03  GRID-HOME-PHONE   PIC X(15). 
      * 
       SCREEN SECTION. 
       01 PgGridSample. 
           03 Scr-Grid, Grid,  
              COL 4.70, LINE 6.00, LINES 26.00 CELLS,  
              SIZE 55.30 CELLS,  
              ADJUSTABLE-COLUMNS, 3-D, COLOR IS 258, COLUMN-HEADINGS, 
              DATA-COLUMNS (1, 21, 41, 45, 60, 75),  
              DISPLAY-COLUMNS (1, 25, 49, 57, 76, 95),  
              ALIGNMENT ("C", "C", "C", "C", "C", "C"),  
              DATA-TYPES ("X(20)", "X(20)", "X(4)", "X(15)", 
                             "X(15)", "X(15)"),  
              SEPARATION (5, 5, 5, 5, 5, 5),  
              COLUMN-DIVIDERS (1, 1, 1, 1, 1, 1),  
              CURSOR-COLOR 2, CURSOR-FRAME-WIDTH 3, DIVIDER-COLOR 1,  
              HEADING-COLOR 257, HEADING-DIVIDER-COLOR 1, HSCROLL,  
              ID IS 1, NUM-ROWS 20, PAGED, RECORD-DATA Grid-Data,  
              RECORD-TO-ADD Grid-Data, ROW-DIVIDERS 1, TILED-HEADINGS,  
              USE-TAB, VPADDING 80, VIRTUAL-WIDTH 112,  
              EVENT PROCEDURE Grid-Events. 
      * 
       PROCEDURE DIVISION. 
       DECLARATIVES. 
       I-O-ERROR SECTION. 
           USE AFTER STANDARD ERROR PROCEDURE ON I-O. 
       0200-DECL. 
           EXIT. 
       END DECLARATIVES. 
      * 
       Acu-Main-Logic. 
           OPEN I-O SAMPLEGRID. 
           PERFORM Acu-PgGridSample-Scrn. 
           PERFORM Load-Grid. 
           PERFORM Acu-PgGridSample-Proc. 
           CLOSE SAMPLEGRID. 
           STOP RUN. 
           . 
       Load-Grid. 
            MOVE GRID-COLUMN-HEADINGS TO GRID-DATA. 
       
            MODIFY SCR-GRID INSERTION-INDEX = 1  
                RECORD-TO-ADD=GRID-DATA. 
       
            MOVE SPACES TO SAMPLEGRIDKEY. 
       
            START SAMPLEGRID KEY NOT <  SAMPLEGRIDKEY 
               INVALID KEY 
                  EXIT PARAGRAPH 
            END-START. 
            MODIFY SCR-GRID ACTION = ACTION-FIRST-PAGE. 
      * 
       Move-Data-To-Grid. 
            MOVE FIRST-NAME  TO GRID-FIRST-NAME.     
            MOVE LAST-NAME TO GRID-LAST-NAME. 
       
            MOVE SAMPLEGRID-EXTENSION  TO GRID-EXTENSION. 
            MOVE SAMPLEGRID-DEPARTMENT TO GRID-DEPARTMENT. 
            MOVE SAMPLEGRID-EMAIL TO GRID-EMAIL. 
            MOVE SAMPLEGRID-HOME-PHONE TO GRID-HOME-PHONE. 
      * 
       Move-Grid-To-Data. 
            MOVE GRID-FIRST-NAME TO FIRST-NAME. 
            MOVE GRID-LAST-NAME  TO LAST-NAME. 
       
            MOVE GRID-EXTENSION TO SAMPLEGRID-EXTENSION. 
            MOVE GRID-DEPARTMENT TO SAMPLEGRID-DEPARTMENT. 
            MOVE GRID-EMAIL  TO SAMPLEGRID-EMAIL. 
       
            MOVE GRID-HOME-PHONE TO SAMPLEGRID-HOME-PHONE. 
      * 
       Acu-Exit-Rtn. 
           EXIT PROGRAM 
           STOP RUN 
           . 
      * 
       Acu-PgGridSample-Routine. 
           PERFORM Acu-PgGridSample-Scrn. 
           PERFORM Acu-PgGridSample-Proc 
           . 
       
       Acu-PgGridSample-Scrn. 
           PERFORM Acu-PgGridSample-Create-Win. 
           DISPLAY PgGridSample UPON Form1-Handle 
           . 
       
       Acu-PgGridSample-Create-Win. 
      * display screen 
           DISPLAY Standard GRAPHICAL WINDOW 
              LINES 38.00, SIZE 64.00, CELL HEIGHT 10, CELL WIDTH 10,  
              AUTO-RESIZE, COLOR IS 65793, ERASE, LABEL-OFFSET 0,  
              LINK TO THREAD, MODELESS, NO SCROLL, WITH SYSTEM MENU,  
              TITLE "Paged Grid Sample", TITLE-BAR, NO WRAP,  
              EVENT PROCEDURE Form1-Event-Proc,  
              HANDLE IS Form1-Handle. 
      * 
           DISPLAY PgGridSample UPON Form1-Handle 
           . 
       
      * PgGridSample 
       Acu-PgGridSample-Proc. 
           PERFORM UNTIL Exit-Pushed 
              ACCEPT PgGridSample 
                 ON EXCEPTION PERFORM Acu-PgGridSample-Evaluate-Func 
              END-ACCEPT 
           END-PERFORM 
           DESTROY Form1-Handle 
           INITIALIZE Key-Status 
           . 
       
      * PgGridSample 
       Acu-PgGridSample-Evaluate-Func. 
      * avoid changing focus 
           MOVE 1 TO Accept-Control 
           . 
        Form1-Event-Proc. 
      * 
           EVALUATE Event-Type 
           WHEN Msg-Close 
              PERFORM Acu-Exit-Rtn 
           END-EVALUATE 
           . 
       
       Grid-Events. 
      * 
          EVALUATE Event-Type 
          WHEN Msg-Paged-First 
             PERFORM Scr-Grid-Ev-Msg-Paged-First 
          WHEN Msg-Paged-Last 
             PERFORM Scr-Grid-Ev-Msg-Paged-Last 
          WHEN Msg-Paged-Next 
             PERFORM Scr-Grid-Ev-Msg-Paged-Next 
          WHEN Msg-Paged-Prev 
             PERFORM Scr-Grid-Ev-Msg-Paged-Prev 
          END-EVALUATE 
          . 
      * 
       Scr-Grid-Ev-Msg-Paged-Next. 
           PERFORM EVENT-DATA-2 TIMES 
             READ SAMPLEGRID NEXT RECORD 
               AT END MOVE EVENT-ACTION-FAIL TO EVENT-ACTION 
               EXIT PARAGRAPH 
             END-READ 
           END-PERFORM 
       
           PERFORM MOVE-DATA-TO-GRID  
           MODIFY SCR-GRID, RECORD-TO-ADD = GRID-DATA 
           . 
      * 
       Scr-Grid-Ev-Msg-Paged-Prev. 
           PERFORM EVENT-DATA-2 TIMES 
             READ SAMPLEGRID PREVIOUS RECORD 
               AT END  
                 MOVE EVENT-ACTION-FAIL TO EVENT-ACTION  
                 EXIT PARAGRAPH 
               END-READ        
           END-PERFORM. 
       
           PERFORM MOVE-DATA-TO-GRID. 
           MODIFY SCR-GRID,  
             INSERTION-INDEX = 2, RECORD-TO-ADD = GRID-DATA 
           . 
      * 
       Scr-Grid-Ev-Msg-Paged-First. 
           MOVE LOW-VALUES TO SAMPLEGRIDKEY. 
           START SAMPLEGRID KEY >= SAMPLEGRIDKEY 
             INVALID KEY MOVE EVENT-ACTION-FAIL TO EVENT-ACTION 
           END-START 
           . 
      * 
       Scr-Grid-Ev-Msg-Paged-Last. 
           MOVE HIGH-VALUES TO SAMPLEGRIDKEY 
           START SAMPLEGRID KEY <= SAMPLEGRIDKEY 
             INVALID KEY MOVE EVENT-ACTION-FAIL TO EVENT-ACTION 
           END-START 
           . 
      
