      *{Bench}prg-comment
      * X-TEXT.cbl
      * X-TEXT.cbl is generated from C:\SM4\SM4GUI\X-TEXT.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. X-TEXT.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 15:36:39.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end


       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT iofile
                  ASSIGN TO fnam
                  organization is line sequential
                  FILE STATUS IS fs.


      *{Bench}file-control
      *{Bench}end
       DATA                        DIVISION.
       FILE                        SECTION.
       FD  iofile 
       01  f-line pic x(100).


      *{Bench}file
      *{Bench}end
       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

      *copy "X-TEXT.cpy"  .

      *{Bench}copy-working
       COPY "X-TEXT.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
      *{Bench}end

       copy "X-TEXT.cpy"  .

       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "X-TEXT.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end




       Main Section.

      *    sprache f ohne extra . todo !!!
      *     string "." "D" into flang
      *     move "../ECRAND/" to foutpath  | todo
      *
      *     move "0204AAAA" to fnam         | todo
      *     move 1 to x-text-mode

           evaluate x-text-mode  

              when 0 

                 move fnam to fnamcpy
                 string fnamcpy flang delimited by " " into fnam
                 perform getscreen
                 move fnamcpy(1:8) to fnam

              when 1

                 perform createcobol

           end-evaluate

      *    move fline(03)(05:08) to h-str                                              

           Exit Program.
           Stop Run.


       getscreen section.

           initialize fcont 
           move 1 to fcnt
           
            open input iofile
            
            perform until fs <> "00" 
            
                  initialize f-line  
                  
                  read iofile 
                        end
                        continue
                        not end      
                        move f-line to fline( fcnt )  
                        add 1 to fcnt
                  end-read                                                        
            
            end-perform
            
            close iofile

           .


       getscreenpara section.

           open input iofile

           move 0 to h-cnt

           perform until fs <> "00" 

		       initialize f-line  
			
               read iofile 
                   end
                       continue
			       not end   
                       if f-line <> spaces
                             add 1 to h-cnt
      				       unstring f-line delimited by  " "
                                 into    h-control-nam(h-cnt) 
                                         h-control-para(h-cnt)
                                 on overflow 
                                     continue
                        end-if

			   end-read                                                        
			
           end-perform

           close iofile
           .


       createcobol section.

           move fnam to fnamcpy

           string fnamcpy flang delimited by " " into fnam
 
           perform getscreen

           string fnamcpy ".txt" delimited by " " into fnam
           perform getscreenpara

           string foutpath fnamcpy "-cpy.cbl.txt" 
               delimited by " " into fnam

      *    verarbeitung 

           open output iofile

           initialize f-line
           write f-line
           move "      * DO NOT CHANGE THIS FILE" to f-line
           write f-line
           write f-line
           write f-line
           initialize f-line
           write f-line

      *     string "initscreen-" fnamcpy "."
      *         delimited by " " 
      *         into h-tmp
      *     string "       " h-str  into f-line
      *     write f-line
      *
      *     move "         ." to f-line
      *     write f-line
      *
           initialize f-line
           write f-line

           string "setlabeltitles-" fnamcpy 
               delimited by " " 
               into h-tmp

           string h-tmp "." delimited by " " into h-str

           string "       " h-str into f-line
           write f-line

           initialize f-line
           write f-line

           perform varying h-idx from 1 by 1 until h-idx > h-cnt 

               initialize h-str h-tmp

               move h-row(h-idx) to h-r
               move h-col(h-idx) to h-c
               move h-len(h-idx) to h-l

               move fline(h-r)(h-c:h-l) to f-line 
               move f-line to h-str

               initialize h-tmp
               string h-control-nam(h-idx) |"-title" 
               delimited by " "
               into h-tmp 

               initialize f-line
               string "      *    move '" fline(h-r)(h-c:h-l) 
               "' to " h-tmp
               into f-line

               write f-line

      *         string "           move fline(" h-r ")("h-c":"h-l")" 
      *         " to " h-tmp
      *         into f-line
      *
      *         write f-line

               initialize f-line
               string "           modify " h-tmp(1:35)
                     into f-line
               write f-line

               initialize f-line
               string "            , TITLE fline(" h-r ")("h-c":"h-l")" 
               into f-line
               write f-line

               initialize f-line
               write f-line

           end-perform

           move "        ." to f-line
           write f-line

           close iofile

           .






       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "X-TEXT.prd".
       COPY "X-TEXT.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
