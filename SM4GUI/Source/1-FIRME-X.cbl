       IDENTIFICATION DIVISION.
       PROGRAM-ID.                      1-FIRME-X.
       INSTALLATION.                    comment-entry.
       DATE-WRITTEN.                    2008/XX/XX - 99:99:99.
       DATE-COMPILED.                   2008/XX/XX - 00:00:00.

      ******************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.                 computer-name.
       OBJECT-COMPUTER.                 computer-name.
      *SPECIAL-NAMES.
      *   CURSOR     IS  cursor-name,
      *   CRT STATUS IS  crt-status-name.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           COPY "FIRME.FC".
      *     SELECT [OPTIONAL] file-name
      *            ASSIGN TO [device] [file-spec]
      *            FILE STATUS IS status-variable.
      *I-O-CONTROL.
      *      i-o-control-entry

      *****************************************************************
       DATA DIVISION.
       FILE SECTION.
      * FD  file-name [IS EXTERNAL] [IS GLOBAL]
      *   [ file-desc { record-description } ...] ... ]
      * SD  file-name
      *   [ sort-desc { record-description } ...]
           COPY "FIRME.FDE".

       WORKING-STORAGE SECTION.
      *For C$CALLERR routine to get why the CALL statement Failed.
       77  ERR-CODE                      pic x(2).
       77  ERR-MESSAGE                   pic x(80).
      *For C$RERR routine to get the extendeed file status information.
       01  EXTEND-STAT.
           03 primary-error              pic x(2).
           03 secondary-error            pic x(10).
       01  TEXT-MESSAGE                  pic x(40).
      *For C$RERRNAME routine to get the last file used in an I/O statement.
       77  LAST-FILENAME                 PIC X(40).


       01  H-FIRME-RECORD.
              03 H-FIRME-KEY              PIC 9(8).
              03 H-FIRME-NOM              PIC X(40).
              03 H-FIRME-LOCALITE         PIC X(50).

       01 HE-REG.
           02 H-R PIC Z(4) OCCURS 20.

           COPY "V-VAR.CPY".

           COPY "FUSER.REC".
           COPY "USER.REC".

       01  CRIT-FLAG             PIC 9 VALUE 0.
       01  CRITERES-1            PIC X(50).
       01  HLP-CRIT REDEFINES CRITERES-1.
           03 CRIT               PIC X OCCURS 50.

       01  TST                  PIC X(4).

       01  ITEMS.
           02 ITEM-A OCCURS 10.
              03 ITEM-IDX        PIC 99.
              03 ITEM            PIC X(10).
              03 ITEM-A REDEFINES ITEM.
                 04 ITEM-2       PIC X(2).
                 04 ITEM-F2      PIC X(8).
              03 ITEM-B REDEFINES ITEM.
                 04 ITEM-3       PIC X(3).
                 04 ITEM-F3      PIC X(7).
              03 ITEM-C REDEFINES ITEM.
                 04 ITEM-4       PIC X(4).
                 04 ITEM-F4      PIC X(6).
              03 ITEM-D REDEFINES ITEM.
                 04 ITEM-5       PIC X(5).
                 04 ITEM-F5      PIC X(5).
              03 ITEM-E REDEFINES ITEM.
                 04 ITEM-6       PIC X(6).
                 04 ITEM-F6      PIC X(4).
              03 ITEM-F REDEFINES ITEM.
                 04 ITEM-7       PIC X(7).
                 04 ITEM-F7      PIC X(3).
              03 ITEM-G REDEFINES ITEM.
                 04 ITEM-8       PIC X(8).
                 04 ITEM-F8      PIC X(2).
              03 ITEM-H REDEFINES ITEM.
                 04 ITEM-9       PIC X(9).
                 04 ITEM-F9      PIC X(1).

       01  CHOIX                 PIC 9(4).
       01  CHOIX-A               PIC X(50).
       01  CHOIX-A1 REDEFINES CHOIX-A.
           02 CHOIX-1            PIC 9.
           02 CHOIX-F1           PIC X(9).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A2 REDEFINES CHOIX-A.
           02 CHOIX-2            PIC 9(2).
           02 CHOIX-F2           PIC X(8).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A3 REDEFINES CHOIX-A.
           02 CHOIX-3            PIC 9(3).
           02 CHOIX-F3           PIC X(7).
           02 CHOIX-FX           PIC X(40).
       01  CHOIX-A4 REDEFINES CHOIX-A.
           02 CHOIX-4            PIC 9(4).
           02 CHOIX-F4           PIC X(6).
           02 CHOIX-FX           PIC X(40).

       01  COMPTEUR              PIC 99.
       01  PRECISION             PIC 9.


       LINKAGE SECTION.
      * [ Data Desciption Entry ....]

           copy "V-LINK.cpy".
           copy "xcall.cpy".

       SCREEN SECTION.
      * [ Screen Description Entry ... ]

      ******************************************************************
       PROCEDURE DIVISION USING LINK-V X-CALL.
      *        [ {USING | CHAINING} {parameter} ... ] .


           evaluate x-call-mode
              when x-call-open
                  move 66 to EXC-KEY
                  move "A" to A-N
                  move 0 to x-call-status
                  initialize fr-matchcode

              when x-call-first
                  perform nextitem

              when x-call-next
                  perform nextitem

              when x-call-close
                   continue
                 
              when x-call-gettabs
                 move 8 to x-data(1)
                 move 9 to x-disp(1)
      
                 move 40  to x-data(2)
                 move 30 to x-disp(2)
      
                 move 50 to x-data(3)
                 move 40 to x-disp(3)
      
                 move 3 to x-tabs-cnt

                 move "01" to x-caption( 1 )
                 move "#nom#" to x-caption( 2 )
                 move "*data*" to x-caption( 3 )
                   
               when x-call-setparas

                 move x-para-cnt to x-para-cnt

                 continue
                 
           end-evaluate

           Exit Program.
      *    Stop Run.

       nextitem.

           initialize x-call-para

              if x-para-cnt = 0 
                 perform next-firme
              else
                 move x-par( 1 ) to fr-matchcode
                 perform Read-Fr
              end-if

              if FS-FIRME = "00" 

                    move FIRME-KEY to H-FIRME-KEY
                    move FIRME-NOM to H-FIRME-NOM
                    move FIRME-LOCALITE to H-FIRME-LOCALITE
      
                    move H-FIRME-RECORD to x-call-return

      *             perform next-firme
              end-if

              move FS-FIRME to x-call-status                

           .

       NEXT-FIRME.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 3
              MOVE "A" TO A-N.
           MOVE FIRME-RECORD TO FR-RECORD.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           MOVE FR-RECORD TO FIRME-RECORD.

           .


       READ-FR.
           MOVE 66 TO EXC-KEY.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           MOVE FR-RECORD TO FIRME-RECORD.
           MOVE FR-KEY TO FUS-FIRME.
           CALL "6-FUSER" USING LINK-V FUS-RECORD FAKE-KEY.
           IF LNK-VAL > PRECISION 
           OR FUS-YN = "N"
              GO READ-FR
           END-IF.
           IF LNK-PRESENCE = 1
           AND FR-FIN-A > 0
           AND FR-FIN-A < LNK-ANNEE
              GO READ-FR
           END-IF.
           IF FR-KEY < 1
             | PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
             | PERFORM INTERRUPT THRU INTERRUPT-END
              IF EXC-KEY = 66
                 INITIALIZE HE-REG IDX-1
                 GO READ-FR
              END-IF
              GO READ-FR-END.
           IF CRIT-FLAG = 1
              PERFORM RECH-CRIT
              IF INPUT-ERROR = 1
                 GO READ-FR
              END-IF
           END-IF.
            | PERFORM DIS-DET-LIGNE.
      *     IF LIN-IDX > 21
      *        PERFORM INTERRUPT THRU INTERRUPT-END
      *        IF EXC-KEY = 66 
      *        OR EXC-KEY = 65 
      *        OR EXC-KEY = 13 
      *           INITIALIZE HE-REG IDX-1
      *           GO READ-FR
      *        END-IF
      *        IF CHOIX NOT = 0
      *           GO READ-FR-END
      *        END-IF
      *     END-IF.
           GO READ-FR.
       READ-FR-END.
           IF CHOIX = 0
              MOVE 5 TO LNK-NUM
              |PERFORM DISPLAY-MESSAGE
      *        IF EXC-KEY NOT = 82 AND NOT = 68
      *           ADD 1 TO LIN-IDX      
      *           PERFORM CLEAN-SCREEN UNTIL LIN-IDX > 21
      *           PERFORM INTERRUPT THRU INTERRUPT-END
      *        END-IF
           END-IF.



       RECH-CRIT.
           INSPECT FIRME-RECORD CONVERTING
           "abcdefghijklmnopqrstuvwxyz�������������������.,;-_'+&*!?" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZCAAAEEEEIIOOUUUOUAE           ".
           MOVE 0 TO INPUT-ERROR IDX-3.
           PERFORM TEST-CRIT THRU TEST-CRIT-END.

        TEST-CRIT.
           ADD 1 TO IDX-3.
           IF ITEM-IDX(IDX-3) < 2
              GO TEST-CRIT-END
           END-IF.
           MOVE 0 TO IDX.
           EVALUATE ITEM-IDX(IDX-3)
              WHEN 2 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-2(IDX-3)
              WHEN 3 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-3(IDX-3)
              WHEN 4 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-4(IDX-3)
              WHEN 5 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-5(IDX-3)
              WHEN 6 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-6(IDX-3)
              WHEN 7 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-7(IDX-3)
              WHEN 8 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-8(IDX-3)
              WHEN 9 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM-9(IDX-3)
              WHEN 10 INSPECT FIRME-RECORD TALLYING IDX FOR CHARACTERS 
              BEFORE ITEM(IDX-3)
           END-EVALUATE.
           IF IDX > 850
              MOVE 1 TO INPUT-ERROR
              GO TEST-CRIT-END
           END-IF.
           IF IDX-3 < 10
              GO TEST-CRIT.
        TEST-CRIT-END.
           EXIT.
                        
       HOMOGENIZE.
           MOVE CHOIX-A TO CRITERES-1.
           INSPECT CRITERES-1 CONVERTING
           "abcdefghijklmnopqrstuvwxyz�������������������.,;-_'+&*!?" TO
           "ABCDEFGHIJKLMNOPQRSTUVWXYZCAAAEEEEIIOOUUUOUAE           ".
           MOVE 1 TO IDX-1 IDX-2 IDX.
           PERFORM PROPER-E THRU PROPER-END.

        PROPER-E.
           IF CRIT(IDX) > SPACES 
           AND IDX-1 < 11
              STRING CRIT(IDX) DELIMITED BY SIZE INTO ITEM(IDX-2) WITH 
              POINTER IDX-1 ON OVERFLOW CONTINUE
           ELSE
              IF IDX-1 > 2
                 ADD 1 TO IDX-2
               END-IF
               MOVE 1 TO IDX-1
           END-IF.
           COMPUTE ITEM-IDX(IDX-2) = IDX-1 - 1.
           ADD 1 TO IDX.
           IF IDX < 51
           AND IDX-2 < 11
              GO PROPER-E.
        PROPER-END.
           EXIT.
 




      * DECLARATIVES.
      * section-name SECTION [ segment-no].
      *    declarative-sentence
      * paragraph-name.
      *     sentence ...   ...
      * END DECLARATIVES.
      
      * Section-name SECTION [ segment-no].
      *    declarative-sentence
      * Paragraph-name.
      *     sentence ...   ...
       Main Section.

           Exit Program.
           Stop Run.

       Library-Routines Section.
      * Library Routines Description

       Display-Call-Err-Message.
           CALL "C$CALLERR" USING ERR-CODE, ERR-MESSAGE
           DISPLAY MESSAGE BOX
                   ERR-MESSAGE,
                   TITLE IS "Call Statement Failed".
                   
       Display-Extended-File-Status.
           CALL "C$RERR" USING EXTEND-STAT, TEXT-MESSAGE.
           CALL "C$RERRNAME" USING LAST-FILENAME.
           MOVE SPACES TO ERR-MESSAGE.
           String "File Name :" delimited by size
                  LAST-FILENAME delimited by spaces
                  " Status ["   delimited by size
                  primary-error delimited by size
                  ","           delimited by size
                  secondary-error delimited by spaces
                  "] "          delimited by size
                  TEXT-MESSAGE  delimited by size
              into ERR-MESSAGE
           End-String.
           DISPLAY MESSAGE BOX
                   ERR-MESSAGE,
                   TITLE IS "File Status".
                   
       Display-File-Status-Codes.
           Evaluate ERR-CODE
               When "00"
                  Display Message "Operation successful."
               When "10"
                  Display Message "End of file."
               When "22"
                  Display Message "Duplicate key found but not allowed."
               When "23"
                  Display Message "Record not found."
               When "34"
                  Display Message "Disk full for sequential file or sort
      -                             " file."
               When "35"
                  Display Message "File not found."
               When "41"
                  Display Message "File is already open."
               When "42"
                  Display Message "File not open."
               When "30"
               When "39"
               When other
                  Perform Display-Extended-File-Status
           End-Evaluate.
