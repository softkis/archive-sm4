      *{Bench}prg-comment
      * MENU.cbl
      * MENU.cbl is generated from C:\SM4\SM4GUI\MENU.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. MENU.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Montag, 1. Juni 2009 13:37:32.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
           COPY "MENU.FC".
           COPY "FIRME.FC".
           COPY "USER.FC".
       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
           COPY "MENU.FDE".
           COPY "FIRME.FDE".
           COPY "USER.FDE".

       WORKING-STORAGE             SECTION.
           COPY "V-LINK.CPY".
           COPY "V-VAR.CPY".
           COPY "PARMOD.REC".
           COPY "MENU.LNK".
           COPY "POCL.REC".
           
           copy "varlist.cpy" .
           copy "1-firme-x-var.cpy" .

      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end

      *{Bench}copy-working
       COPY "MENU.wrk".
      *{Bench}end

sm3
       01  ECR-DISPLAY.
           02 HE-Z3 PIC Z(3).
           02 HE-Z4 PIC Z(4).
           02 HE-ZX REDEFINES HE-Z4.
               03 HE-M PIC Z.
               03 HE-F PIC Z.
               03 HE-A PIC ZZ.
           02 HE-DATE.
               03 HE-JJ PIC ZZ.
               03 HE-FIL PIC X VALUE ".".
               03 HE-MM PIC ZZ.
               03 HE-FIL PIC X VALUE ".".
               03 HE-AA PIC ZZZZ.
           02 HE-Z6 PIC Z(6). 

       01  H-M.
           02 HELP-MENU OCCURS 15.
              03 HELP-KEY.
                 04 HELP-LEVEL.
                    05 HELP-LEV          PIC 99 OCCURS 5.

              03 HELP-BODY.
                 04 HELP-PROG-NAME       PIC X(8).
                 04 HELP-PROG-NAME-R REDEFINES HELP-PROG-NAME.
                    05 HELP-PROG-BEG     PIC XX.
                    05 HELP-PROG-END     PIC X(6).
                 04 HELP-COMPETENCE      PIC 9.
                 04 HELP-CALL            PIC X(8).
                 04 HELP-PASSWORD        PIC X(22).
                 04 HELP-DESCRIPTION     PIC X(40).
                 04 HELP-TEXT            PIC 9(8).
                 04 HELP-PROG-NUMBER     PIC 9(8).
                 04 HELP-EXTENSION-1     PIC X(8).
                 04 HELP-EXTENSION-2     PIC X(8).
                 04 HELP-TEST            PIC 9.
                 04 HELP-BATCH           PIC 9.
                 04 HELP-LOG             PIC 9.
                 04 HELP-FILLER          PIC X(20).

      * 0-mgr 
          
       01  CALL-WORD.
           02 CW-1               PIC XX.
           02 CW-2               PIC X(8).
       01  CALL-WORD-R REDEFINES CALL-WORD.
           02 CW-0               PIC X.
           02 CW-9               PIC X(9).
       01  CALL-WORD-R1 REDEFINES CALL-WORD.
           02 CW-3               PIC X.
           02 CW-4               PIC X.
           02 CW-5               PIC X(8).

       01  X-CALLWORD            PIC X(8).
       01  IDX-LEVEL             PIC 99 COMP-6 VALUE 1.
       01  CHOIX                 PIC 99.
                 
sm3
           01 wrk-today-date     pic x(10).

           01 h-lev              pic 9(10).
           01 h-levels redefines h-lev  .
              02 h-l             pic 99 occurs 5.

           01 h-lev-cpy          pic 9(10).
           01 h-l-c redefines h-lev-cpy.
              02 h-lc             pic 99 occurs 5.

           01 h-idx              pic 99.

           01 h-num              pic 99.

           01 h-company          pic 99999.
           01 h-prgid            pic 99 value 0.
           01 h-menuid           pic 99.
           01 h-go-exit          pic 9 value 0.

           01 h-lang-input       pic 9 value 0.

sm3
       01   H-FIRME-RECORD.
              03 H-FIRME-KEY             PIC 9(8).
              03 H-FIRME-NOM             PIC X(40).
              03 H-FIRME-LOCALITE        PIC X(50).

       01   H-MENU-RECORD.
              03 H-MENU-KEY              PIC Z9.
              03 H-MENU-description      PIC X(40).
              03 H-MENU-call             PIC X(8).

       01   h-menu-hlp.
           02   H-MENU-stack occurs 5.
                03 H-stack-KEY             PIC Z9.
                03 H-stack-description     PIC X(40).


sm3

logg
           01 h-log-user pic x(10).
           01 h-log-pass pic x(10).
           01 h-stat-bar pic x(100).
           01 h-log-ok pic 9 value 0.

           01  X-PASSWORD            PIC X(22).
           01  XX-PASSWORD           PIC X(22).


       LINKAGE                     SECTION.
      *{Bench}linkage
      *{Bench}end

      *    copy "xcall.cpy"


       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "MENU.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end


       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-MENUS-Routine
      *{Bench}end
      * MENU
           PERFORM Acu-Exit-Rtn
           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "MENU.prd".
       COPY "MENU.evt".
      *{Bench}end


       openfile.
           OPEN INPUT USER
           open INPUT MENU 
           INITIALIZE MN-BODY
           .

       closefile.
           close MENU
           close user
           .

       getlicense.
           INITIALIZE PARMOD-RECORD PC-RECORD.
           MOVE "LICENSE" TO PARMOD-MODULE
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-PROG-NUMBER-1 TO LNK-LICENSE PC-NUMBER.
           MOVE 1 TO PC-FIRME.
           CALL "6-LIC" USING LINK-V PC-RECORD "N" FAKE-KEY.
           move PC-NOM to H-FIRME-NOM LNK-NOM.
           IF  PARMOD-PROG-NUMBER-1 = 0
           AND TODAY-ANNEE > 2007
           AND TODAY-MOIS  > 3
              COMPUTE LNK-TOP = TODAY-ANNEE - 2
           END-IF.
           IF PC-FIN-A > 0
              MOVE PC-FIN-A TO LNK-TOP
           END-IF.

           IF LNK-LICENSE > 1
              INITIALIZE PARMOD-RECORD
              MOVE "UPDATE" TO PARMOD-MODULE
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R"
              IF PARMOD-PROG-NUMBER-1 < 20081207
                 CALL "U-CODSAL" USING LINK-V
                 CANCEL "U-CODSAL" 
                 CALL "U-CSMOD" USING LINK-V
                 CANCEL "U-CSMOD" 
                 CALL "AD2009" USING LINK-V
                 CANCEL "AD2009" 
                 CALL "CC2009" USING LINK-V
                 CANCEL "CC2009" 
                 CALL "MEN2009" USING LINK-V
                 CANCEL "MEN2009"
                 CALL "CMAL2009" USING LINK-V
                 CANCEL "CMAL2009"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20081213
                 CALL "U-OCC" USING LINK-V
                 CANCEL "U-OCC" 
                 CALL "U-OCCSU" USING LINK-V
                 CANCEL "U-OCCSU" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20081214
                 CALL "CSFP" USING LINK-V
                 CANCEL "CSFP" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090101
                 CALL "SEC" USING LINK-V
                 CANCEL "SEC"
                 IF PC-FLAG-09 NOT = 0
                    CALL "3-FACASS" USING LINK-V PC-RECORD
                    CANCEL "3-FACASS" 
                 END-IF
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090102
                 MOVE 20090102 TO PARMOD-PROG-NUMBER-1
                 CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
                 MOVE "AA" TO LNK-AREA
                 MOVE 0 TO LNK-POSITION
                 MOVE 28 TO LNK-NUM
                 CALL "0-DMESS" USING LINK-V
                 PERFORM AA
                 MOVE 99999998 TO LNK-VAL
                 CALL "0-BOOK" USING LINK-V
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090103
                 CALL "PL2009" USING LINK-V
                 CANCEL "PL2009"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090104
                 CALL "PV-CCM" USING LINK-V
                 CANCEL "PV-CCM"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090105
                 CALL "U-OCCP" USING LINK-V
                 CANCEL "U-OCCP"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090106
                 CALL "U-FICHE" USING LINK-V
                 CANCEL "U-FICHE"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090110
                 CALL "CSCHAM" USING LINK-V
                 CANCEL "CSCHAM" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090111
                 CALL "U-CSCOR" USING LINK-V
                 CANCEL "U-CSCOR" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090112
                 CALL "U-MOD1" USING LINK-V
                 CANCEL "U-MOD1" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090113
                 CALL "U-CIS" USING LINK-V
                 CANCEL "U-CIS" 
              END-IF
              MOVE 20090113 TO PARMOD-PROG-NUMBER-1
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
           END-IF.

           PERFORM AFFICHAGE-ECRAN.           


       AFFICHAGE-ECRAN.
           MOVE "0" TO LNK-LANGUAGE.
           MOVE  0  TO LNK-VAL.
           MOVE "    " TO LNK-AREA.
      *     PERFORM WORK-ECRAN.
           MOVE LNK-LICENSE TO HE-Z4.
           move pc-nom to pc-nom.

       NEXT-FIRME section.
           MOVE "N" TO A-N.
           IF INDICE-ZONE = 3
              MOVE "A" TO A-N.
           MOVE FIRME-RECORD TO FR-RECORD.
           CALL "6-FIRME" USING LINK-V A-N EXC-KEY.
           MOVE FR-RECORD TO FIRME-RECORD.

           .


      * 0-mgr start

       CALL-BY-NAME section.
           MOVE 0 TO INPUT-ERROR.
           PERFORM READ-BY-NAME THRU READ-BY-NAME-END
           MOVE 1 TO INPUT-ERROR.
           
       READ-BY-NAME section.
           MOVE CALL-WORD TO MN-CALL MN-PROG-NAME LNK-TEXT
           READ MENU KEY MN-CALL
                INVALID CONTINUE
                NOT INVALID GO READ-BY-NAME-1.
           READ MENU KEY MN-PROG-NAME
                INVALID CONTINUE
                NOT INVALID GO READ-BY-NAME-1.

                GO READ-BY-NAME-END

               | todo  auswahl prog liste anzeigen
           CALL "2-MENU" USING LINK-V LINK-RECORD
               | todo


           IF LINK-PROG-NAME > SPACES
              MOVE LINK-PASSWORD TO LNK-TEXT
              CALL "0-PASSW" USING LINK-V
              IF LNK-VAL NOT = 0
                 INITIALIZE LINK-RECORD
              END-IF
           END-IF
           IF LINK-PROG-NAME > SPACES
              MOVE LINK-RECORD TO MN-RECORD MENU-RECORD
              MOVE 0 TO LNK-PERSON
              MOVE MN-PROG-NAME TO X-CALLWORD
              IF X-CALLWORD > SPACES

                 |move x-callword to MN-PROG-NAME

      *           CALL X-CALLWORD USING LINK-V
      *           EXCEPTION   
      *             MOVE 1 TO INPUT-ERROR
      *             MOVE "AA" TO LNK-AREA
      *             MOVE 8 TO LNK-NUM
      *             MOVE 0 TO LNK-POSITION
      *            | todo PERFORM DISPLAY-MESSAGE
      *           END-CALL
      *           CANCEL X-CALLWORD

                  GO READ-BY-NAME-END | todo ?

                 PERFORM GET-INDEX-LEVEL
                 MOVE MN-LEV(IDX-LEVEL) TO CHOIX
              END-IF
           END-IF
           CANCEL "2-MENU"
           | todo PERFORM AFFICHAGE-ECRAN.
           PERFORM NEXT-MENU VARYING IDX FROM 1 BY 1 UNTIL IDX  > 15.
           GO READ-BY-NAME-END.
       READ-BY-NAME-1.
           IF LNK-COMPETENCE < MN-COMPETENCE 
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              MOVE 0 TO LNK-POSITION
             | todo PERFORM DISPLAY-MESSAGE
              GO READ-BY-NAME-END.
           | todo MOVE MN-PASSWORD TO LNK-TEXT.
           |todo CALL "0-PASSW" USING LINK-V.
           IF LNK-VAL NOT = 0
              GO READ-BY-NAME-END.
           IF INPUT-ERROR = 0
              MOVE 0 TO LNK-STATUS
              MOVE 0 TO LNK-NUM
              MOVE MN-PROG-NAME TO X-CALLWORD
              IF X-CALLWORD NUMERIC
                 MOVE X-CALLWORD TO LNK-VAL
                 MOVE "0-BOOK" TO X-CALLWORD
              END-IF
              IF LNK-LANGUAGE NOT = "F"
                 CALL "0-MNTXT" USING LNK-LANGUAGE MN-RECORD
              END-IF
              MOVE MN-RECORD TO MENU-RECORD
           END-IF.
           MOVE 0 TO LNK-PERSON LNK-POSITION.
      *     CALL X-CALLWORD USING LINK-V
      *     EXCEPTION 
      *         MOVE "AA" TO LNK-AREA
      *         MOVE 8 TO LNK-NUM
      *         MOVE 0 TO LNK-POSITION
      *         | todo PERFORM DISPLAY-MESSAGE
      *         GO READ-BY-NAME-END
      *     END-CALL.
      *     CANCEL X-CALLWORD.
              move X-CALLWORD to mn-prog-name
              go READ-BY-NAME-END
           PERFORM GET-INDEX-LEVEL.
           MOVE MN-LEV(IDX-LEVEL) TO CHOIX.
           | todo PERFORM AFFICHAGE-ECRAN.
           PERFORM NEXT-MENU VARYING IDX FROM 1 BY 1 UNTIL IDX > 15.
       READ-BY-NAME-END.
           EXIT section.

       
       NEXT-MENU section.
           PERFORM CLEAR-F-LEVEL VARYING IDX-1 FROM IDX-LEVEL BY 1 
           UNTIL IDX-1 > 5.
           MOVE IDX TO MN-LEV(IDX-LEVEL).
           READ MENU INVALID     
                INITIALIZE MN-BODY
           END-READ.
           IF LNK-LANGUAGE NOT = "F"
              CALL "0-MNTXT" USING LNK-LANGUAGE MN-RECORD.
           MOVE MN-RECORD TO HELP-MENU(IDX).
           IF HELP-DESCRIPTION(IDX) = SPACES 
              MOVE 0 TO HELP-TEST(IDX)
           ELSE
              MOVE 1 TO HELP-TEST(IDX). 
           | todo PERFORM DISPLAY-ECRAN.

       GET-INDEX-LEVEL section.
           MOVE 0 TO IDX-LEVEL.
           PERFORM COUNT-STEPS VARYING IDX FROM 1 BY 1 UNTIL IDX > 5.

       CLEAR-F-LEVEL section.
           MOVE 0 TO MN-LEV(IDX-1).

       COUNT-STEPS section.
           IF MN-LEV(IDX) > 0 ADD 1 TO IDX-LEVEL.

      * 0-mgr start
           

      * openfile_USER.
      *     OPEN INPUT USER
      *     INITIALIZE US-REC-DET.



      * closefile_USER.
      *     close USER
      *    STOP RUN.
           .



       COPY "XACTION.CPY".



       REPORT-COMPOSER SECTION.
