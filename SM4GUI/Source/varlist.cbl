      *{Bench}prg-comment
      * varlist.cbl
      * varlist.cbl is generated from C:\SM4\SM4GUI\varlist.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. varlist.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Montag, 1. Juni 2009 12:49:16.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

       01  idx   pic 99.

       01  h-data pic 999.
       01  h-disp pic 999.

       01  h-manuel pic 9.

       01  h-caption pic x(100).

           copy "xcall.cpy"

      *{Bench}copy-working
       COPY "varlist.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
       COPY "varlist.lks".
      *{Bench}end

      *     copy "V-LINK.cpy".
      *     copy "varlist.cpy".


       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "varlist.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION USING LINK-V, var-call.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-varlistselect-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "varlist.prd".
       COPY "varlist.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
