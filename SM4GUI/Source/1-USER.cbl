      *{Bench}prg-comment
      * 1-USER.cbl
      * 1-USER.cbl is generated from C:\SM4\SM4GUI\1-USER.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-USER.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Montag, 1. Juni 2009 13:05:13.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
       COPY "USERS.sl".
      *{Bench}end
       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
       COPY "USERS.fd".
      *{Bench}end
       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "fonts.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end


           COPY "V-VAR.CPY".
           COPY "X-TEXT.cpy"  .
           copy "varlist.cpy" .
           copy "1-firme-x-var.cpy"

       01  user-first-run        PIC 9 value 0.
       01  X-PASSWORD            PIC X(10).

       01  ECR-DISPLAY.
           02 HE-Z4 PIC ZZZZ. 
           02 HE-Z6 PIC Z(6). 
           02 HE-Z2 PIC ZZ. 

      *{Bench}copy-working
       COPY "1-USER.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
           COPY  "V-LINK.CPY".
      *{Bench}linkage
      *{Bench}end
       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-USER.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION.
      *{Bench}end
      *{Bench}declarative
       DECLARATIVES.
       INPUT-ERROR SECTION.
           USE AFTER STANDARD ERROR PROCEDURE ON INPUT.
       0100-DECL.
           EXIT.
       I-O-ERROR SECTION.
           USE AFTER STANDARD ERROR PROCEDURE ON I-O.
       0200-DECL.
           EXIT.
       OUTPUT-ERROR SECTION.
           USE AFTER STANDARD ERROR PROCEDURE ON OUTPUT.
       0300-DECL.
           EXIT.
       USERS-ERROR SECTION.
           USE AFTER STANDARD EXCEPTION PROCEDURE ON USERS.
       END DECLARATIVES.
      *{Bench}end

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-SCR_USER-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

       copy "0002NNNN-cpy.cbl.txt"   .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-USER.prd".
       COPY "1-USER.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
