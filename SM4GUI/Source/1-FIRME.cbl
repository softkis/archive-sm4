      *{Bench}prg-comment
      * 1-FIRME.cbl
      * 1-FIRME.cbl is generated from C:\SM4\SM4GUI\1-FIRME.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-FIRME.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Montag, 1. Juni 2009 09:19:01.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end

           COPY "FIRME.FC".

       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end

           COPY "FIRME.FDE".

       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "fonts.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

           01 firme-first-run pic 9 value 0.
           01 1-firme-first   pic 9.
           01 1-firme-last    pic 9.


           01 title-s1 pic x(30) value "tab1".
           01 title-s2 pic x(30) value "tab2".
           01 title-s3 pic x(30) value "tab3".
           01 title-s4 pic x(30) value "tab4".

           copy "X-TEXT.cpy"  .
           COPY "V-VAR.CPY".

           copy "varlist.cpy" .
           copy "1-firme-x-var.cpy"


      *{Bench}copy-working
       COPY "1-FIRME.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
       COPY "1-FIRME.lks".
      *{Bench}end

           COPY  "V-LINK.CPY".

       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-FIRME.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION USING LINK-V.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

           move 1 to 1-firme-first.
           move 0 to 1-firme-last.
           perform openfile.

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-FIRME-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

           perform closefile.

           copy "0101NNNN-cpy.cbl.txt"   .

           .
      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-FIRME.prd".
       COPY "1-FIRME.evt".
      *{Bench}end

       openfile.
           open I-O FIRME 
           INITIALIZE FIRME-RECORD
           .

       closefile.
           close FIRME
           .

       REPORT-COMPOSER SECTION.
