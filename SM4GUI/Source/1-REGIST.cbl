      *{Bench}prg-comment
      * 1-REGIST.cbl
      * 1-REGIST.cbl is generated from C:\SM4\SM4GUI\1-REGIST.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-REGIST.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 15:36:39.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end

           COPY "REGISTRE.FC".
           COPY "PERSON.FC".

       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end

           COPY "REGISTRE.FDE".
           COPY "PERSON.FDE".


       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "fonts.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

           copy "X-TEXT.cpy"  .
           copy "varlist.cpy" .
           COPY "V-VAR.CPY".
           COPY "PERSON.LK".
           COPY "PERSON.LNK".


           01 regist-first-run pic 9 value 0. 
           01 1-regist-first   pic 9.
           01 1-regist-last    pic 9.

           01 title-s1 pic x(30) value "tab1".
           01 title-s2 pic x(30) value "tab2".
           01 title-s3 pic x(30) value "tab3".
           01 title-s4 pic x(30) value "tab4".

           01 HE-DATE .
              03 HE-JJ PIC 99.
      *        03 FILLER PIC X VALUE ".".
              03 HE-MM PIC 99.
      *        03 FILLER PIC X VALUE ".".
              03 HE-AA PIC 9999.

           01 H-REGIST-DATE-NAISS    PIC X(8).
           01 H-REGIST-DATE-ANCIEN   PIC X(8).



      *{Bench}copy-working
       COPY "1-REGIST.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
       COPY "1-REGIST.lks".
      *{Bench}end
       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-REGIST.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION USING LINK-V.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

           move 1 to 1-regist-first.
           move 0 to 1-regist-last.
           perform openfile.

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-REGIST-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

           perform closefile.

           copy "0204NNNN-cpy.cbl.txt"   .

           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-REGIST.prd".
       COPY "1-REGIST.evt".
      *{Bench}end

       openfile.
           open I-O REGISTRE 
           INITIALIZE REG-RECORD
           .

       closefile.
           close REGISTRE
           .


       REPORT-COMPOSER SECTION.
