      *{Bench}prg-comment
      * 1-MUT.cbl
      * 1-MUT.cbl is generated from C:\SM4\SM4GUI\1-MUT.Psf
      *{Bench}end
       IDENTIFICATION              DIVISION.
      *{Bench}prgid
       PROGRAM-ID. 1-MUT.
       AUTHOR. Mitarbeiter.
       DATE-WRITTEN. Sonntag, 7. Juni 2009 16:36:02.
       REMARKS. 
      *{Bench}end
       ENVIRONMENT                 DIVISION.
       CONFIGURATION               SECTION.
       SPECIAL-NAMES.
      *{Bench}activex-def
      *{Bench}end
      *{Bench}decimal-point
           DECIMAL-POINT IS COMMA.
      *{Bench}end
       INPUT-OUTPUT                SECTION.
       FILE-CONTROL.
      *{Bench}file-control
      *{Bench}end
           COPY "MUT.FC".
       DATA                        DIVISION.
       FILE                        SECTION.
      *{Bench}file
      *{Bench}end
           COPY "MUT.FDE".
       WORKING-STORAGE             SECTION.
      *{Bench}acu-def
       COPY "acugui.def".
       COPY "acucobol.def".
       COPY "crtvars.def".
       COPY "showmsg.def".
      *{Bench}end
      *{Bench}extern-def        
      *{Bench}end

sm3
       01  HE-INPUT-PARAMETER .
      *    LINE COL SIZE HE-SCREEN    LLCCSZECRA
           02 IP-101 PIC x(12)  VALUE "0305250009EE".
           02 IP-102 PIC x(12)  VALUE "0405250057EE".
           02 IP-103 PIC x(12)  VALUE "0505251023EE".
           02 IP-DEC PIC x(12)  VALUE "2305250099EE".

       01  IP-PAR REDEFINES HE-INPUT-PARAMETER.
            03 I-PP OCCURS 4.
               04 IP-LINE  PIC 99.
               04 IP-COL   PIC 99.
               04 IP-SIZE  PIC 99.
               04 IP-ECRAN PIC 9999.
               04 IP-AREA PIC xx.

       01  CHOIX-MAX       PIC 99 VALUE 4.

      *    +-------------------------------+
      *    �  Variables pour ce programme  �
      *    +-------------------------------+

           COPY "V-VAR.CPY".
       01  SAVE-FIRME      PIC 9(6).

sm3

       01  h-text-mut pic x(70).
       01  h-text-mut2 pic x(73).
       01  h-text-idx pic z(4).

      *{Bench}copy-working
       COPY "1-MUT.wrk".
      *{Bench}end
       LINKAGE                     SECTION.
      *{Bench}linkage
       COPY "1-MUT.lks".
      *{Bench}end


       SCREEN                      SECTION.
      *{Bench}copy-screen
       COPY "1-MUT.scr".
      *{Bench}end

      *{Bench}linkpara
       PROCEDURE DIVISION USING LINK-V.
      *{Bench}end
      *{Bench}declarative
      *{Bench}end

       Acu-Main-Logic.
      *{Bench}entry-befprg
      *    Before-Program
      *{Bench}end
           PERFORM Acu-Initial-Routine
      * run main screen
      *{Bench}run-mainscr
           PERFORM Acu-MUT-Routine
      *{Bench}end
           PERFORM Acu-Exit-Rtn
           .

      *{Bench}copy-procedure
       COPY "showmsg.cpy".
       COPY "1-MUT.prd".
       COPY "1-MUT.evt".
      *{Bench}end
       REPORT-COMPOSER SECTION.
