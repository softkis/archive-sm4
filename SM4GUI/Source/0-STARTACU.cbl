      *  +-------------------------------------------------------+
      *  � PROGRAMME 0-START PROGRAMME INIT                      �
      *  +-------------------------------------------------------+

       IDENTIFICATION DIVISION.

       PROGRAM-ID.    0-START.

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
      *--------------------
       FILE-CONTROL.

      *    COPY "USER.FC".
      ******** Ersetzen COPY "USER.FC" durch direkte Anweisung aus 
      ******** COPY-Modul
           SELECT USER   
                  ASSIGN TO RANDOM, "H-USER"
                  ORGANIZATION INDEXED, 
                  ACCESS DYNAMIC,
                  RECORD KEY US-KEY, 
                  STATUS FS-USER. 
          
      *    COPY "MENU.FC".
      ******** Ersetzen COPY "MENU.FC" durch direkte Anweisung aus 
      ******** COPY-Modul
           SELECT OPTIONAL MENU ASSIGN TO RANDOM, "S-MENU",
                  ORGANIZATION is INDEXED,
                  ACCESS is DYNAMIC,
                  RECORD KEY is MN-KEY,
                  ALTERNATE RECORD KEY is MN-CALL, DUPLICATES,
                  ALTERNATE RECORD KEY is MN-PROG-NAME, DUPLICATES,
                  STATUS FS-MENU.
                  
       DATA DIVISION.

       FILE SECTION.
      *------------
      *    COPY "USER.FDE".
      ******** Ersetzen COPY "USER.FDE" durch direkte Anweisung aus 
      ******** COPY-Modul (USER.REC in USER.FDE integriert !!!!!)

       FD  USER
           LABEL RECORD STANDARD
           DATA RECORD US-RECORD.

      * Beginn COPY "USER.REC"
       01  US-RECORD.
           02  US-KEY                  PIC X(10).

           02  US-REC-DET.
             03  US-COMPETENCE           PIC 9.
             03  US-PASSWORD             PIC X(22).
             03  US-DESCR                PIC X(30).
             03  US-COLOR.
                 04  US-COL              PIC 9 OCCURS 3.
             03  US-LANGUAGE             PIC X.
             03  US-FIRME                PIC 9(6).
             03  US-PERSON               PIC 9(8).
             03  US-FILLER               PIC X(36).
      * ENDE COPY "USER.REC"
             
      *    COPY "MENU.FDE".
      ******** Ersetzen COPY "MENU.FDE" durch direkte Anweisung aus 
      ******** COPY-Modul (MENU.REC in MENU.FDE integriert !!!!!)
       FD  MENU
           LABEL RECORD STANDARD
           DATA RECORD MN-RECORD.

      * Beginn COPY "MENU.REC"
       01  MN-RECORD.
           02  MN-KEY PIC 9(10).
           02  MN-LEVEL REDEFINES MN-KEY.
                   04  MN-LEV              PIC 99 OCCURS 5.

           02  MN-BODY.
               03  MN-PROG-NAME            PIC X(8).
               03  MN-PROG-NAME-R REDEFINES MN-PROG-NAME.
                   04 MN-PROG-BEG          PIC XX.
                   04 MN-PROG-END          PIC X(6).
               03  MN-COMPETENCE           PIC 9.
               03  MN-CALL                 PIC X(8).
               03  MN-PASSWORD             PIC X(22).
               03  MN-DESCRIPTION          PIC X(40).
               03  MN-HELP-TEXT            PIC 9(8).
               03  MN-PROG-NUMBER          PIC 9(8).
               03  MN-PROG-NUMBERS REDEFINES MN-PROG-NUMBER.
                   04 MN-PROG-NUM          PIC 9 OCCURS 8.
               03  MN-EXTENSION-1          PIC X(8).
               03  MN-EXTENSION-2          PIC X(8).
               03  MN-TEST                 PIC 9.
               03  MN-BATCH                PIC 9.
               03  MN-LOG                  PIC 9.
               03  MN-FILLER               PIC X(20).
      * ENDE COPY "MENU.REC"


       WORKING-STORAGE SECTION.
      *-----------------------
acu
       77  char-win-handle
                  USAGE IS HANDLE OF WINDOW.
       77  start-tb-1-handle
                  usage is handle of window.  
       77  char-font-handle
                  USAGE IS HANDLE OF FONT.
       copy "fonts.def".
acu

           COPY "V-VAR.CPY".
           COPY "FUSER.REC".
           COPY "PARMOD.REC".
           COPY "MESSAGE.REC".
           COPY "PAYS.REC".
           COPY "POCL.REC".
 
       01  CHOIX-MAX             PIC 99 VALUE 6. 
       01  CHOIX                 PIC 9.
       01  SEMAINE               PIC 9.
       01  X-PASSWORD            PIC X(22).
       01  XX-PASSWORD           PIC X(22).

       01  ECR-DISPLAY.
           02 HE-Z3 PIC Z(3).
           02 HE-Z4 PIC Z(4).
           02 HE-ZX REDEFINES HE-Z4.
               03 HE-M PIC Z.
               03 HE-F PIC Z.
               03 HE-A PIC ZZ.
           02 HE-DATE.
               03 HE-JJ PIC ZZ.
               03 HE-FIL PIC X VALUE ".".
               03 HE-MM PIC ZZ.
               03 HE-FIL PIC X VALUE ".".
               03 HE-AA PIC ZZZZ.
           02 HE-Z6 PIC Z(6).

      *LINKAGE SECTION.
      
      *    COPY "V-LINK.CPY".
      ******** Ersetzen COPY "V-LINK.CPY" durch direkte Anweisung aus 
      ******** COPY-Modul (V-ERR.CPY/FIRME.LNK in V-LINK.CPY integriert)
       01  LINK-V.
           02 LNK-USER       PIC X(10).
           02 LNK-LANGUAGE   PIC XX.
           02 LG-IDX         PIC 99.
           02 LNK-AREA       PIC XXXX.
           02 LNK-COMPETENCE PIC 9.
           02 LNK-DATE.
              03 LNK-ANNEE  PIC 9(4).
              03 LNK-ANNEE-R REDEFINES LNK-ANNEE.
                 04 LNK-MILLE   PIC 9.
                 04 LNK-SUFFIX  PIC 999.
              03 LNK-ANNEE-S REDEFINES LNK-ANNEE.
                 04 LNK-SIECLE   PIC 99.
                 04 LNK-DECENNIE PIC 99.
              03 LNK-MOIS   PIC 99.
              03 LNK-JOUR   PIC 99.
           02 LNK-DATE-DEVIANT.
              03 LNK-ANNEE-D PIC 9(4).
              03 LNK-ANNEE-DR REDEFINES LNK-ANNEE-D.
                 04 LNK-MILLE-D  PIC 9.
                 04 LNK-SUFFIX-D PIC 999.
              03 LNK-MOIS-D  PIC 99.
           02 LNK-SUITE      PIC 99.
           02 S-I.
              03 SEM-MOIS OCCURS 12.
                 04 SEM-IDX     PIC  9 OCCURS 31.
                 04 SEM-NUM     PIC 99 OCCURS 31.
                 04 SEM-LINE    PIC  9 OCCURS 31.
           02 MOIS-ANNEE        PIC X(24).
           02 MOIS-JOURS REDEFINES MOIS-ANNEE.
              03 MOIS-JRS PIC 99 OCCURS 12.
           02 INDEXES.
              03 MOIS-IDX PIC 999V9(8) OCCURS 12.
              03 MOIS-SALMIN PIC 9(5)V9(4) OCCURS 12.
           02 INDEX-F REDEFINES INDEXES.
              03 MOIS-TX PIC 99999V9(6) OCCURS 12.
              03 MOIS-SALMINR PIC 9(5)V9(4) OCCURS 12.

           02 LNK-MATRICULE  PIC 9(11).
           02 LNK-KEY        PIC 9(8).
           02 LNK-PERSON     PIC 9(8).
           02 LNK-AB         REDEFINES LNK-PERSON.
              03 LNK-A       PIC 9(4).
              03 LNK-B       PIC 9(4).
           02 LNK-NOM        PIC X(60).
           02 LNK-NOM-R1     REDEFINES LNK-NOM.       
              03 LNK-MATR       PIC 9(13).
              03 LNK-FILL       PIC X(47).
           02 LNK-NOM-R      REDEFINES LNK-NOM.       
              03 LNK-TEXT       PIC X(50).
              03 LNK-NUM        PIC 9(6).
              03 LNK-CALL-1     PIC 99.    
              03 LNK-CALL-2     PIC 99.
           02 LNK-NOM-R2     REDEFINES LNK-NOM.       
              03 LNK-TEXT-1     PIC X(20).
              03 LNK-TEXT-2     PIC X(20).
              03 LNK-TEXT-3     PIC X(20).
           02 LNK-STATUS     PIC 99.
           02 LNK-VAL        PIC S9(8)V9(4) COMP-3.
           02 LNK-VAL-2      PIC S9(8)V9(4) COMP-3.
           02 LNK-INDEX      PIC 99.
           02 LNK-CHOIX      PIC 99.
           02 LNK-YN         PIC X.
           02 LNK-POSITION   PIC 9(8).
           02 LNK-POSITION-R REDEFINES LNK-POSITION.
              03 LNK-LINE    PIC 99.
              03 LNK-COL     PIC 99.
              03 LNK-SIZE    PIC 99.
              03 LNK-SPACE   PIC 99.

           02 LNK-LOW        PIC X.
           02 LNK-EXC-KEY    PIC 9(4) COMP-1.
           02 LNK-A-N        PIC X.
           02 LNK-PRESENCE   PIC 9.
ANNEE      02 LNK-LIMITE     PIC 9999.
           02 LNK-COLOR1     PIC 9.
           02 LNK-COLOR2     PIC 9.
           02 LNK-COLOR3     PIC 9.
           02 LNK-FIRME      PIC 9(6).
           02 LNK-REGISTRE   PIC 9(8).
ANNEE      02 LNK-TOP        PIC 9999.
           02 LNK-TODAY      PIC 9(8).
           02 LNK-TODAY-R REDEFINES LNK-TODAY.
              03 LNK-AA  PIC 9(4).
              03 LNK-MM  PIC 99.
              03 LNK-JJ  PIC 99.
           02 LNK-LICENSE    PIC 9999.
           02 LNK-FILLER     PIC X(11).
           02 LNK-LOG        PIC X.
           02 LNK-SQL        PIC X.

      *    Copie du fichier menu 
      
           02 MENU-RECORD.
              03 MENU-KEY.
                 04 MENU-LEVEL.
                    05 MENU-LEV      PIC 99 OCCURS 5.

              03 MENU-BODY.
                 04 MENU-PROG-NAME   PIC X(8).
                 04 MENU-PROG-NAME-R REDEFINES MENU-PROG-NAME.
                    05 MENU-PROG-BEG PIC XX.
                    05 MENU-PROG-END PIC X(6).
                 04 MENU-COMPETENCE  PIC 9.
                 04 MENU-CALL        PIC X(8).
                 04 MENU-PASSWORD    PIC X(22).
                 04 MENU-DESCRIPTION PIC X(40).
                 04 MENU-HELP-TEXT   PIC 9(8).
                 04 MENU-PROG-NUMBER PIC 9(8).
                 04 MENU-PROG-NUMBERS REDEFINES MENU-PROG-NUMBER.
                    05 MENU-PROG-NUM PIC 9 OCCURS 8.
                 04 MENU-EXTENSION-1 PIC X(8).
                 04 MENU-EXTENSION-2 PIC X(8).
                 04 MENU-TEST        PIC 9.
                 04 MENU-BATCH       PIC 9.
                 04 MENU-LOG         PIC 9.
                 04 MENU-FILLER      PIC X(20).
      * BEGIN COPY "V-ERR.CPY"
       02  FS-ERROR.
1          03 FS-AGENCE     PIC XX.
2          03 FS-ATTENTE    PIC XX.
3          03 FS-DOCUMENT   PIC XX.
4          03 FS-CONTACT    PIC XX.
5          03 FS-POCL       PIC XX.
6          03 FS-BANQF      PIC XX.
7          03 FS-BANQP      PIC XX.
8          03 FS-BANQUE     PIC XX.
9          03 FS-PLAN       PIC XX.
10         03 FS-COMPTE     PIC XX.
11         03 FS-CODTEXT    PIC XX.
12         03 FS-CALEN      PIC XX.
13         03 FS-CARRIERE   PIC XX.
14         03 FS-CCOL       PIC XX.
15         03 FS-CDPOST     PIC XX.
16         03 FS-CONTRAT    PIC XX.
17         03 FS-REGIME     PIC XX.
18         03 FS-CONGE      PIC XX.
19         03 FS-CODFIX     PIC XX.
20         03 FS-CODSAL     PIC XX.
21         03 FS-CODPAR     PIC XX.
22         03 FS-CODCOPY    PIC XX.
23         03 FS-CONTYPE    PIC XX.
24         03 FS-FUSER      PIC XX.
25         03 FS-COUT       PIC XX.
26         03 FS-DIS        PIC XX.
27         03 FS-DIVIS      PIC XX.
28         03 FS-CCC        PIC XX.
29         03 FS-CREDIT     PIC XX.
30         03 FS-METIER     PIC XX.
31         03 FS-QUALIF     PIC XX.
32         03 FS-EQUIPE     PIC XX.
33         03 FS-LAYOUT     PIC XX.
34         03 FS-CCM        PIC XX.
35         03 FS-FACTURE    PIC XX.
36         03 FS-FICHE      PIC XX.
37         03 FS-FIRME      PIC XX.
38         03 FS-FISC       PIC XX.
39         03 FS-TAUXCC     PIC XX.
40         03 FS-BAREME     PIC XX.
41         03 FS-BARDEF     PIC XX.
42         03 FS-FAMILLE    PIC XX.
43         03 FS-COMPP      PIC XX.
44         03 FS-CPFIX      PIC XX.
45         03 FS-HORSEM     PIC XX.
46         03 FS-CONV       PIC XX.
47         03 FS-PLANDEF    PIC XX.
48         03 FS-IMPOT      PIC XX.
49         03 FS-IMPLOG     PIC XX.
50         03 FS-IMPSYS     PIC XX.
51         03 FS-IMPTYP     PIC XX.
52         03 FS-FID        PIC XX.
53         03 FS-INDEX      PIC XX.
54         03 FS-LOG        PIC XX.
55         03 FS-JOURNAL    PIC XX.
56         03 FS-LIBELLE    PIC XX.
57         03 FS-MISSION    PIC XX.
58         03 FS-LIVRE      PIC XX.
59         03 FS-MEMO       PIC XX.
60         03 FS-MOTDEP     PIC XX.
61         03 FS-JOURS      PIC XX.
62         03 FS-HEURES     PIC XX.
63         03 FS-PLANS      PIC XX.
64         03 FS-OCCUP      PIC XX.
65         03 FS-OCCOM      PIC XX.
66         03 FS-OCCTEXT    PIC XX.
67         03 FS-RUBRIQUE   PIC XX.
68         03 FS-PAIE       PIC XX.
69         03 FS-PALIER     PIC XX.
70         03 FS-PAYS       PIC XX.
71         03 FS-PERSON     PIC XX.
72         03 FS-POSTE      PIC XX.
73         03 FS-PRIME      PIC XX.
74         03 FS-ETAPE      PIC XX.
75         03 FS-INTER      PIC XX.
76         03 FS-POSE       PIC XX.
77         03 FS-PREV       PIC XX.
78         03 FS-RAISON     PIC XX.
79         03 FS-COMPETE    PIC XX.
80         03 FS-SPOOLM     PIC XX.
81         03 FS-MINUTE     PIC XX.
82         03 FS-SYNDICAT   PIC XX.
83         03 FS-STATUT     PIC XX.
84         03 FS-RAPPORT    PIC XX.
85         03 FS-PCREF      PIC XX.
86         03 FS-TRI        PIC XX.
87         03 FS-REGION     PIC XX.
88         03 FS-TAUX       PIC XX.
89         03 FS-TAUXSS     PIC XX.
90         03 FS-TXACC      PIC XX.
91         03 FS-MALADIE    PIC XX.
92         03 FS-VOITURE    PIC XX.
93         03 FS-DIPLOME    PIC XX.
94         03 FS-MENUTEXT   PIC XX.
95         03 FS-VIREM      PIC XX.
96         03 FS-MENU       PIC XX.
97         03 FS-USER       PIC XX.
98         03 FS-MESSAGE    PIC XX.
99         03 FS-HELP       PIC XX.
100        03 FS-PRINTER    PIC XX.
101        03 FS-MENUC      PIC XX.
102        03 FS-LIVREARC   PIC XX.
103        03 FS-CODARC     PIC XX.
104        03 FS-GEOREG     PIC XX.
105        03 FS-BANQC      PIC XX.
106        03 FS-REGISTRE   PIC XX.
107        03 FS-POINT      PIC XX.
108        03 FS-LEX        PIC XX.
109        03 FS-PLAGE      PIC XX.
110        03 FS-PROFESSION PIC XX.
111        03 FS-QUADEF     PIC XX.
112        03 FS-HRANN      PIC XX.
113        03 FS-CSPDET     PIC XX.
114        03 FS-DECMAL     PIC XX.
115        03 FS-SNOCS      PIC XX.
116        03 FS-SPOOLQ     PIC XX.
117        03 FS-PARMOD     PIC XX.
118        03 FS-IMPTAB     PIC XX.
119        03 FS-FORM       PIC XX.
120        03 FS-OBJET      PIC XX.
121        03 FS-TRANS      PIC XX.
122        03 FS-PICKUP     PIC XX.
123        03 FS-HOPSEM     PIC XX.
124        03 FS-DEVISE     PIC XX.
125        03 FS-HPJOUR     PIC XX.
126        03 FS-LIVANN     PIC XX.
127        03 FS-TRACT      PIC XX.
128        03 FS-BANQCL     PIC XX.
129        03 FS-DOMAINE    PIC XX.
130        03 FS-CANDIDAT   PIC XX.
131        03 FS-CRITERE    PIC XX.
132        03 FS-DOSSIER    PIC XX.
133        03 FS-PROFILE    PIC XX.
134        03 FS-EVAL       PIC XX.
135        03 FS-POSTES     PIC XX.
136        03 FS-EVEN       PIC XX.
137        03 FS-DEMANDE    PIC XX.
138        03 FS-SECTEUR    PIC XX.
139        03 FS-FIRMAT     PIC XX.
140        03 FS-FNAME      PIC XX.
141        03 FS-ORTKZ      PIC XX.
142        03 FS-CSDEF      PIC XX.
143        03 FS-143        PIC XX.
144        03 FS-144        PIC XX.
145        03 FS-145        PIC XX.
146        03 FS-146        PIC XX.
147        03 FS-147        PIC XX.
148        03 FS-148        PIC XX.
149        03 FS-149        PIC XX.
150        03 FS-TVA        PIC XX.
151        03 FS-PRIX       PIC XX.
152        03 FS-ARTICLE    PIC XX.
153        03 FS-ECHEANCE   PIC XX.
154        03 FS-VIRDEB     PIC XX.
155        03 FS-VIRCRE     PIC XX.
156        03 FS-CREART     PIC XX.
157        03 FS-157        PIC XX.
158        03 FS-158        PIC XX.
159        03 FS-159        PIC XX.
160        03 FS-PERS-ARCH  PIC XX.
161        03 FS-REG-ARCH   PIC XX.
162        03 FS-CON-ARCH   PIC XX.
163        03 FS-CAR-ARCH   PIC XX.
164        03 FS-FICHE-ARCH PIC XX.
165        03 FS-CG-ARCH    PIC XX.
166        03 FS-BQ-ARCH    PIC XX.
167        03 FS-BQP-ARCH   PIC XX.
168        03 FS-VIR-ARCH   PIC XX.
169        03 FS-MUTU       PIC XX.
170        03 FS-170        PIC XX.
171        03 FS-171        PIC XX.
172        03 FS-172        PIC XX.
173        03 FS-173        PIC XX.
174        03 FS-174        PIC XX.
175        03 FS-175        PIC XX.
176        03 FS-176        PIC XX.
177        03 FS-177        PIC XX.
178        03 FS-178        PIC XX.
179        03 FS-179        PIC XX.
180        03 FS-180        PIC XX.
181        03 FS-181        PIC XX.
182        03 FS-182        PIC XX.
183        03 FS-183        PIC XX.
184        03 FS-184        PIC XX.
185        03 FS-185        PIC XX.
186        03 FS-186        PIC XX.
187        03 FS-187        PIC XX.
188        03 FS-188        PIC XX.
189        03 FS-189        PIC XX.
190        03 FS-FIRME-ARCH PIC XX.
191        03 FS-CC-ARCH    PIC XX.
192        03 FS-BQF-ARCH   PIC XX.
193        03 FS-193        PIC XX.
194        03 FS-194        PIC XX.
195        03 FS-195        PIC XX.
196        03 FS-196        PIC XX.
197        03 FS-197        PIC XX.
198        03 FS-198        PIC XX.
199        03 FS-199        PIC XX.
200        03 FS-200        PIC XX.
       02  FS-ERROR-R REDEFINES FS-ERROR.
           03 FS-ERR        PIC XX OCCURS 200.

       02  EXTENDED-STATUS PIC 99V99.
      * END COPY "V-ERR.CPY"


      * BEGIN COPY "FIRME.LNK"
       02  FR-RECORD.
           03 FR-KEY-ALPHA.
              04 FR-MATCHCODE        PIC X(10).
              04 FR-KEY              PIC 9(8).

           03 FR-REC-DET.
              04 FR-PAYS             PIC XXX.
              04 FR-CODE-POST        PIC 9(8).
              04 FR-CP REDEFINES FR-CODE-POST.
                 05 FR-CP-FILL       PIC 9999.
                 05 FR-CP-LUX        PIC 9999.
              04 FR-CP1 REDEFINES FR-CODE-POST.
                 05 FR-CP-FILL       PIC 999.
                 05 FR-CP5           PIC 9(5).
              04 FR-NOM              PIC X(40).
              04 FR-RUE              PIC X(40).
              04 FR-MAISON           PIC X(10).
              04 FR-LOCALITE         PIC X(50).
              04 FR-GESTION          PIC X(8).
              04 FR-CORRESPONDANT    PIC X(20).
              04 FR-COURRIER-1       PIC X(50).
              04 FR-COURRIER-2       PIC X(50).
              04 FR-COURRIER-3       PIC X(50).

              04 FR-ACTIVITE         PIC X(50).
              04 FR-PHONE            PIC X(20).
              04 FR-FAX              PIC X(20).
              04 FR-FISC-BUREAU      PIC X(10).
              04 FR-FISC-CODE        PIC 9.

              04 FR-PASSWORD         PIC X(22).
              04 FR-COMPETENCE       PIC 9.

              04 FR-DATE-DEB         PIC 9(8).
              04 FR-DATE-DEB-R REDEFINES FR-DATE-DEB.
                 05 FR-DEB-A         PIC 9999.
                 05 FR-DEB-M         PIC 99.
                 05 FR-DEB-J         PIC 99.
              04 FR-DATE-FIN         PIC 9(8).
              04 FR-DATE-FIN-R REDEFINES FR-DATE-FIN.
                 05 FR-FIN-A         PIC 9999.
                 05 FR-FIN-M         PIC 99.
                 05 FR-FIN-J         PIC 99.

              04 FR-DATE-ETAB.
                 05 FR-ETAB          PIC 9(13).
              04 FR-MATRICULE REDEFINES FR-DATE-ETAB.
                 05 FR-ETAB-A        PIC 9999.
                 05 FR-ETAB-N        PIC 9999.
                 05 FR-SNOCS         PIC 999.
                 05 FR-EXTENS        PIC 99.
              04 FR-MATRICULE-R REDEFINES FR-DATE-ETAB.
                 05 FR-MAT           PIC 9 OCCURS 13.
              04 FR-MATRICULE-P REDEFINES FR-DATE-ETAB.
                 05 FR-PERS          PIC 9(11).
                 05 FR-EXTENS-P      PIC 99.

              04 FR-FISC.
                 05 FR-FISC-A        PIC 9999.
                 05 FR-FISC-N        PIC 9999.
                 05 FR-FISC-M        PIC 999.

              04 FR-CONTREPARTIE     PIC X(8).
              04 FR-FREQ-COMPTA      PIC 99.
              04 FR-CLIENT-COMPTA    PIC 9(8).
              04 FR-REMARQUE         PIC X(50).
              04 FR-LOGO             PIC X(40).
              04 FR-FIP-POSTE        PIC X.

              04 FR-SNOCS-YN         PIC X.
              04 FR-ACCIDENT         PIC 99.
              04 FR-SANTE            PIC X.

              04 FR-FREQ-FACT        PIC 99.
              04 FR-IMP-CONGE        PIC X.
              04 FR-LANGUE           PIC XX.
              04 FR-CODPAR           PIC 99.
              04 FR-BASE-IMPOT       PIC 9.
              04 FR-ETAPES           PIC X.
              04 FR-PRIME            PIC 99.
              04 FR-CLIENT-YN        PIC X.
              04 FR-TF-COMPTA        PIC X(8).
              04 FR-IMP-RECUP        PIC X.
              04 FR-PORTO            PIC X.
              04 FR-FORM-SAL         PIC X.
              04 FR-EMAIL            PIC X(40).
              04 FR-TVA              PIC X(20).
              04 FR-IMP-FERIE        PIC X.
              04 FR-IMP-REPOS        PIC X.
              04 FR-IMP-MOBIL        PIC X.
              04 FR-TRANSPORT        PIC X.
              04 FR-FIP-LOGO         PIC X.
              04 FR-RESPONSABLE      PIC 9(11).
              04 FR-TYPE             PIC 9.
              04 FR-METIER           PIC X(10).
              04 FR-NOM-JF           PIC X.
              04 FR-COMMERCE         PIC X(15).
              04 FR-CCM              PIC X.
              04 FR-ECHEANCE         PIC 999.
              04 FR-COMPTA           PIC X(15).
              04 FR-ARRONDI          PIC X.
              04 FR-ANNEE-ARRONDI    PIC 9999.
              04 FR-TAUX-FEDIL       PIC 99.
              04 FR-ENVELOPPE        PIC X.
              04 FR-IMP-JOURS        PIC X.
              04 FILLER              PIC X(54).
            03 FR-STAMP.
                 04 FR-TIME.
                    05 FR-ST-ANNEE   PIC 9999.
                    05 FR-ST-MOIS    PIC 99.
                    05 FR-ST-JOUR    PIC 99.
                    05 FR-ST-HEURE   PIC 99.
                    05 FR-ST-MIN     PIC 99.
                 04 FR-USER          PIC X(10).      
      * END COPY "FIRME.LNK"
      *****Definition Toolbar *****************
      * 01 start-tb-1.
      *     03 start-tb-1, push-button, 
      *        col 57,00, line 1,5, Lines 2,00 cells, size 8,00 cells,
      *        exception-value 27, id is 1, self-act, 
      *        title "ENDE".

                  
           COPY "controls.def".


       PROCEDURE DIVISION.

       DECLARATIVES.
      
       FILE-ERR-PROC SECTION.
       
           USE AFTER ERROR PROCEDURE ON USER.
       
       FILE-ERROR-PROC.
      
           IF FS-USER  = "35"
              CALL "1-USER" USING LINK-V 
           ELSE
              CALL "C$RERR" USING EXTENDED-STATUS
              CALL "0-ERROR" USING LINK-V
              STOP RUN
           END-IF.
      
       END DECLARATIVES.

       START-DISPLAY SECTION.
             
       START-PROGRAMME-0-START.
acu
           initialize wfont-data
           move "Courier New" to wfont-name
           move 16 to wfont-size
           call "w$font" using wfont-get-closest-font, 
                               char-font-handle, 
                               wfont-data

      ********** URSPRUNG DISPLAY ******************
      *     display standard window
      *        font char-font-handle
      *        lines 26, size 80
      *        title "SM4"
      *        handle in char-win-handle.

           display standard graphical window
              font char-font-handle
              lines 26, size 80
              title "SM4", color is 65793,
              handle in char-win-handle.

           display TOOL-BAR, color is 160,
              handle is start-tb-1-handle
           
      *     display start-tb-1 upon start-tb-1-handle  

 
            DISPLAY LIST-BOX, LINE 5, COL 30, LINES 5. 
            MODIFY CONTROL, LINE 5, COL 30,  
               PROPERTY LBP-ITEM-TO-ADD =  
                        ( "Softkis", "FRIK" ). 


      *     DISPLAY Standard GRAPHICAL WINDOW
      *        SCREEN LINE 50, SCREEN COLUMN 50, 
      *        LINES 48,00, SIZE 64,00, CELL HEIGHT 10, 
      *        CELL WIDTH 10, AUTO-MINIMIZE, COLOR IS 65793, 
      *        LABEL-OFFSET 0, LINK TO THREAD, MODELESS, NO SCROLL, 
      *        WITH SYSTEM MENU, 
      *        TITLE 
      *        "SM4 by SOFT-KIS                 Tel.: 328380 Fax: 3279
   -  *        "84        info@softkis.lu", TITLE-BAR, USER-GRAY, 
      *        USER-WHITE, NO WRAP, 
      *        EVENT PROCEDURE MENUS-Event-Proc, 
      *        HANDLE IS MENUS-Handle

acu     
           INITIALIZE LINK-V LNK-POSITION MN-RECORD.
           CALL "6-MENU" USING LINK-V MN-RECORD NX-KEY.
           IF MN-CALL = "SQL"
              CALL "9-DBTEST" USING LINK-V 
              MOVE "Y" TO LNK-SQL.
           IF MN-EXTENSION-1 = "LOG"
              MOVE "Y" TO LNK-LOG.
           MOVE "312831303130313130313031" TO MOIS-ANNEE.
           MOVE 3 TO LG-IDX.
           MOVE SPACES TO LNK-AREA.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-DATE  TO LNK-TODAY
           MOVE TODAY-ANNEE TO LNK-ANNEE
           MOVE TODAY-MOIS  TO LNK-MOIS.
           MOVE TODAY-JOUR  TO LNK-JOUR.
           COMPUTE LNK-TOP = TODAY-ANNEE + 10.

           CALL   "0-JRSEM" USING LINK-V.
           CANCEL "0-JRSEM".
           MOVE SEM-IDX(LNK-MOIS, LNK-JOUR) TO SEMAINE.
           CALL   "1-LINDEX" USING LINK-V.
           CANCEL "1-LINDEX".
           MOVE 2001 TO LNK-LIMITE.

           OPEN INPUT USER.
           INITIALIZE PARMOD-RECORD PC-RECORD.
           MOVE "LICENSE" TO PARMOD-MODULE
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           MOVE PARMOD-PROG-NUMBER-1 TO LNK-LICENSE PC-NUMBER.
           MOVE 1 TO PC-FIRME.
           CALL "6-LIC" USING LINK-V PC-RECORD "N" FAKE-KEY.
           IF  PARMOD-PROG-NUMBER-1 = 0
           AND TODAY-ANNEE > 2007
           AND TODAY-MOIS  > 3
              COMPUTE LNK-TOP = TODAY-ANNEE - 2
           END-IF.
           IF PC-FIN-A > 0
              MOVE PC-FIN-A TO LNK-TOP
           END-IF.

           IF LNK-LICENSE > 1
              INITIALIZE PARMOD-RECORD
              MOVE "UPDATE" TO PARMOD-MODULE
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R"
              IF PARMOD-PROG-NUMBER-1 < 20081207
                 CALL "U-CODSAL" USING LINK-V
                 CANCEL "U-CODSAL" 
                 CALL "U-CSMOD" USING LINK-V
                 CANCEL "U-CSMOD" 
                 CALL "AD2009" USING LINK-V
                 CANCEL "AD2009" 
                 CALL "CC2009" USING LINK-V
                 CANCEL "CC2009" 
                 CALL "MEN2009" USING LINK-V
                 CANCEL "MEN2009"
                 CALL "CMAL2009" USING LINK-V
                 CANCEL "CMAL2009"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20081213
                 CALL "U-OCC" USING LINK-V
                 CANCEL "U-OCC" 
                 CALL "U-OCCSU" USING LINK-V
                 CANCEL "U-OCCSU" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20081214
                 CALL "CSFP" USING LINK-V
                 CANCEL "CSFP" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090101
                 CALL "SEC" USING LINK-V
                 CANCEL "SEC"
                 IF PC-FLAG-09 NOT = 0
                    CALL "3-FACASS" USING LINK-V PC-RECORD
                    CANCEL "3-FACASS" 
                 END-IF
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090102
                 MOVE 20090102 TO PARMOD-PROG-NUMBER-1
                 CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
                 MOVE "AA" TO LNK-AREA
                 MOVE 0 TO LNK-POSITION
                 MOVE 28 TO LNK-NUM
                 CALL "0-DMESS" USING LINK-V
                 PERFORM AA
                 MOVE 99999998 TO LNK-VAL
                 CALL "0-BOOK" USING LINK-V
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090103
                 CALL "PL2009" USING LINK-V
                 CANCEL "PL2009"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090104
                 CALL "PV-CCM" USING LINK-V
                 CANCEL "PV-CCM"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090105
                 CALL "U-OCCP" USING LINK-V
                 CANCEL "U-OCCP"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090106
                 CALL "U-FICHE" USING LINK-V
                 CANCEL "U-FICHE"
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090110
                 CALL "CSCHAM" USING LINK-V
                 CANCEL "CSCHAM" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090111
                 CALL "U-CSCOR" USING LINK-V
                 CANCEL "U-CSCOR" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090112
                 CALL "U-MOD1" USING LINK-V
                 CANCEL "U-MOD1" 
              END-IF
              IF PARMOD-PROG-NUMBER-1 < 20090113
                 CALL "U-CIS" USING LINK-V
                 CANCEL "U-CIS" 
              END-IF
              MOVE 20090113 TO PARMOD-PROG-NUMBER-1
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
           END-IF.

           PERFORM AFFICHAGE-ECRAN.

           PERFORM TRAITEMENT-ECRAN THRU TRAITEMENT-ECRAN-END
           UNTIL INDICE-ZONE > CHOIX-MAX.

      *  +-------------------------------------------------------+
      *  �       creation window                                 �      
      *  +-------------------------------------------------------+

       TRAITEMENT-ECRAN.
      *----------------- 

           IF INDICE-ZONE < 1 MOVE 1 TO INDICE-ZONE.
           
           INITIALIZE EXC-KEY-CTL.

      * param�tres g�n�rales touches de fonctions

           MOVE 0000130000 TO EXC-KFR (3).
           MOVE 0027000000 TO EXC-KFR (6).
           MOVE 0052530000 TO EXC-KFR (11).

      * param�tres sp�cifiques touches de fonctions

           EVALUATE INDICE-ZONE
           WHEN 3  MOVE 0000000065 TO EXC-KFR (13) 
                   MOVE 6600000000 TO EXC-KFR (14) 
           WHEN 4  MOVE 0000000065 TO EXC-KFR (13) 
                   MOVE 6600000000 TO EXC-KFR (14) 
           WHEN 5  MOVE 0063640015 TO EXC-KFR (1)
                   MOVE 0000000065 TO EXC-KFR (13) 
                   MOVE 6600000000 TO EXC-KFR (14) 
           WHEN 6  MOVE 0000000000 TO EXC-KFR (1)
                   MOVE 0040000000 TO EXC-KFR (2)
                   MOVE 0052000000 TO EXC-KFR (11)
                   MOVE 0000680000 TO EXC-KFR (14). 

           PERFORM DISPLAY-F-KEYS.

       TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM AVANT-1 
           WHEN  2 PERFORM AVANT-2 
           WHEN  3 PERFORM AVANT-3 
           WHEN  4 PERFORM AVANT-4 
           WHEN  5 PERFORM AVANT-5 
           WHEN  6 PERFORM AVANT-DEC.

           DISPLAY SPACES LINE 22 POSITION 2 SIZE 70.
           EVALUATE EXC-KEY
                WHEN 82 GO END-PROGRAM
                WHEN 98 GO TRAITEMENT-ECRAN-01
                WHEN 52 MOVE 27 TO EXC-KEY
                WHEN 53 MOVE 13 TO EXC-KEY
           END-EVALUATE.
           INITIALIZE INPUT-ERROR.
           IF EXC-KEY-FUN(EXC-KEY) = 0 GO TRAITEMENT-ECRAN-01.

           EVALUATE INDICE-ZONE
           WHEN  1 PERFORM APRES-1 
           WHEN  2 PERFORM APRES-2 
           WHEN  3 PERFORM APRES-3 
           WHEN  4 PERFORM APRES-4 
           WHEN  5 PERFORM APRES-5 
           WHEN  6 PERFORM APRES-6. 

           EVALUATE EXC-KEY
                WHEN 27 SUBTRACT 1 FROM INDICE-ZONE
                WHEN 54 MOVE CHOIX-MAX TO INDICE-ZONE
                WHEN 13 IF INPUT-ERROR = 0
                           ADD 1 TO INDICE-ZONE
                        END-IF.

           IF INDICE-ZONE > CHOIX-MAX MOVE CHOIX-MAX TO INDICE-ZONE.

       TRAITEMENT-ECRAN-END.

      *=================================================================
      *  Fonction : traitement avant l'accept d'une zone     
      *=================================================================

       AVANT-1.            
           ACCEPT LNK-USER
             LINE 16 POSITION 38 SIZE 10
             TAB UPDATE NO BEEP CURSOR  1
             CONTROL "UPPER"
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF LNK-USER = "19511117JL"
           AND EXC-KEY = 5
              CALL "1-USER" USING LINK-V

              PERFORM AFFICHAGE-ECRAN
              INITIALIZE LNK-USER
           GO AVANT-1.
           IF EXC-KEY = 12
           OR EXC-KEY = 1
              IF LNK-LICENSE < 1
              OR LNK-LICENSE = 2008
                 PERFORM AVANT-LICENSE
              END-IF
           END-IF.
           IF EXC-KEY = 19
              PERFORM AVANT-LICENSE
           END-IF.
           IF EXC-KEY = 20
              INITIALIZE PARMOD-RECORD
              MOVE "STOP" TO PARMOD-MODULE
              MOVE LNK-ANNEE TO PARMOD-PROG-NUMBER-1
              SUBTRACT 1 FROM PARMOD-PROG-NUMBER-1
              MOVE 12  TO PARMOD-PROG-NUMBER-2
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
              MOVE 13 TO EXC-KEY
              INITIALIZE PARMOD-RECORD
           END-IF.

       AVANT-2.            
           IF  X-PASSWORD NOT = SPACES
           ACCEPT XX-PASSWORD
             LINE 17 POSITION 38 SIZE 10
             TAB NO BEEP CURSOR  1 OFF
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-3. 
           ACCEPT LNK-ANNEE
             LINE 18 POSITION 38 SIZE 4 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-4. 
           ACCEPT LNK-MOIS
             LINE 19 POSITION 40 SIZE 2 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.
           IF EXC-KEY = 15
              INITIALIZE PARMOD-RECORD
              MOVE "STOP" TO PARMOD-MODULE
              MOVE LNK-ANNEE TO PARMOD-PROG-NUMBER-1
              MOVE LNK-MOIS  TO PARMOD-PROG-NUMBER-2
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W"
              MOVE 13 TO EXC-KEY
           END-IF.
           IF EXC-KEY = 18
              INITIALIZE PARMOD-RECORD
              MOVE "STOP" TO PARMOD-MODULE
              CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "D"
              INITIALIZE PARMOD-RECORD
              MOVE 13 TO EXC-KEY
           END-IF.

       AVANT-5.            
           IF LNK-FIRME > 0
              MOVE LNK-FIRME TO FR-KEY
           END-IF.
           IF FR-KEY = 0
              MOVE 66 TO EXC-KEY
              PERFORM NEXT-FIRME.
           IF LNK-FIRME = 0
           ACCEPT FR-KEY
             LINE 20 POSITION 36 SIZE 6 
             TAB UPDATE NO BEEP CURSOR  1
             ON EXCEPTION  EXC-KEY  CONTINUE.

       AVANT-LICENSE.      
           ACCEPT LNK-LICENSE
             LINE 12 POSITION 42 SIZE 4
             TAB UPDATE NO BEEP CURSOR 1
             ON EXCEPTION EXC-KEY CONTINUE.
             PERFORM WRITE-LICENSE.

       WRITE-LICENSE.
           MOVE "LICENSE" TO PARMOD-MODULE.
           MOVE LNK-LICENSE TO PARMOD-PROG-NUMBER-1.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "W".

      *=================================================================
      *  Fonction : traitement apres l'accept d'une zone     
      *=================================================================

       APRES-1.
           MOVE LNK-USER TO US-KEY.
           READ USER NO LOCK INVALID 
                INITIALIZE US-REC-DET 
                MOVE 1 TO INPUT-ERROR
                NOT INVALID
                    MOVE US-LANGUAGE   TO LNK-LANGUAGE
                    MOVE US-COMPETENCE TO LNK-COMPETENCE
                    MOVE US-PASSWORD   TO LNK-TEXT
                    MOVE 0 TO LNK-STATUS
                    CALL "0-GARBLE" USING LINK-V
                    MOVE LNK-TEXT TO X-PASSWORD
                END-READ.
           IF US-FIRME < 1
              MOVE 0 TO US-FIRME.
           IF US-PERSON < 1
              MOVE 0 TO US-PERSON.
           MOVE 1 TO LG-IDX.
           EVALUATE LNK-LANGUAGE
                WHEN "D" MOVE 2 TO LG-IDX
                WHEN "E" MOVE 3 TO LG-IDX
           END-EVALUATE.
           IF US-COL(1) = 0
              MOVE 8 TO  US-COL(1).
           IF US-COL(2) = 0
              MOVE 1 TO  US-COL(2).
           IF US-COL(3) = 0
              MOVE 1 TO  US-COL(3).
           IF US-COL(1) = 8 AND US-COL(2) = 1 AND US-COL(3) = 1
              CONTINUE
           ELSE PERFORM MAKE-COLOR.
           MOVE US-COL(1) TO LNK-COLOR1.
           MOVE US-COL(2) TO LNK-COLOR2.
           MOVE US-COL(3) TO LNK-COLOR3.
           MOVE US-FIRME  TO LNK-FIRME.
           MOVE US-PERSON TO LNK-REGISTRE.
           IF  LNK-LICENSE = 0
           AND TODAY-ANNEE > 2007
           AND TODAY-MOIS  > 2
               MOVE "AA" TO LNK-AREA
               MOVE 0 TO LNK-POSITION
               MOVE 9999 TO LNK-NUM
               CALL "0-DMESS" USING LINK-V
               MOVE 1 TO INPUT-ERROR
           END-IF.
           IF PC-FLAG-10 > 0
              MOVE "AA" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              MOVE 9998 TO LNK-NUM
              MOVE PC-FLAG-10 TO LNK-VAL
              CALL "0-DMESS" USING LINK-V
              PERFORM AA
              STOP RUN
           END-IF.
           PERFORM DIS-HE-01.

       APRES-2.
           IF X-PASSWORD  NOT = SPACES
           IF XX-PASSWORD NOT = X-PASSWORD
              MOVE 1 TO INPUT-ERROR
           END-IF.
           
       APRES-3.
           IF EXC-KEY = 65
              SUBTRACT 1 FROM LNK-ANNEE
              MOVE 12 TO LNK-MOIS.
           IF EXC-KEY = 66
              ADD 1 TO LNK-ANNEE
              MOVE 1 TO LNK-MOIS.
           IF LNK-ANNEE < LNK-LIMITE
              MOVE LNK-LIMITE TO LNK-ANNEE
           END-IF.
           IF LNK-ANNEE > LNK-TOP
              MOVE LNK-TOP TO LNK-ANNEE
           END-IF.
           MOVE "STOP" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF  PARMOD-PROG-NUMBER-1 > 0
           AND PARMOD-PROG-NUMBER-1 < LNK-ANNEE
              MOVE PARMOD-PROG-NUMBER-1 TO LNK-ANNEE 
           END-IF.
           INITIALIZE PARMOD-RECORD.

           PERFORM DIS-HE-03.
           
       APRES-4.
           IF EXC-KEY = 65
              SUBTRACT 1 FROM LNK-MOIS.
           IF EXC-KEY = 66
              ADD 1 TO LNK-MOIS.
           IF LNK-MOIS < 1 
              MOVE 1 TO LNK-MOIS
              MOVE 1 TO INPUT-ERROR.
           IF LNK-MOIS > 12
              MOVE 12 TO LNK-MOIS
              MOVE 1 TO INPUT-ERROR.
           MOVE "STOP" TO PARMOD-MODULE.
           CALL "6-PARMOD" USING LINK-V PARMOD-RECORD "R".
           IF  PARMOD-PROG-NUMBER-1 = LNK-ANNEE
           AND PARMOD-PROG-NUMBER-2 < LNK-MOIS
              MOVE PARMOD-PROG-NUMBER-2 TO LNK-MOIS 
           END-IF.
           INITIALIZE PARMOD-RECORD.
           PERFORM DIS-HE-04.
                      
       APRES-5.
           IF LNK-FIRME > 0
              MOVE LNK-FIRME TO FR-KEY
           END-IF.
           EVALUATE EXC-KEY
           WHEN  5 CALL "1-FIRME" USING LINK-V
                   PERFORM AFFICHAGE-ECRAN 
           WHEN  2 THRU 3 
                   IF EXC-KEY = 2
                      MOVE "A" TO LNK-A-N
                   ELSE
                      MOVE "N" TO LNK-A-N
                   END-IF
                   MOVE 1 TO LNK-PRESENCE
                   MOVE " " TO LNK-YN
                   CALL "2-FIRME" USING LINK-V
                   IF LNK-YN = "X" 
                      MOVE 13 TO EXC-KEY 
                   END-IF
                   MOVE 0 TO LNK-PRESENCE
                   PERFORM AFFICHAGE-ECRAN.
           PERFORM NEXT-FIRME.
           PERFORM AFFICHAGE-DETAIL.
           MOVE FR-KEY TO FUS-FIRME.
           CALL "6-FUSER" USING LINK-V FUS-RECORD FAKE-KEY.
           IF FR-COMPETENCE > LNK-COMPETENCE
           OR FUS-YN = "N"
              MOVE "AA" TO LNK-AREA
              MOVE 4 TO LNK-NUM
              PERFORM DISPLAY-MESSAGE
              MOVE 1 TO INPUT-ERROR
           END-IF.
           IF FUS-YN = "Y"
              MOVE FUS-COMPETENCE TO LNK-COMPETENCE
           ELSE
              MOVE US-COMPETENCE TO LNK-COMPETENCE
           END-IF.
           IF FR-KEY = 0
              MOVE 1 TO INPUT-ERROR
           ELSE
              IF EXC-KEY = 2
              OR EXC-KEY = 3
                 ADD 1 TO INDICE-ZONE
              END-IF
           END-IF.
           IF INPUT-ERROR = 0
           AND EXC-KEY NOT = 65
           AND EXC-KEY NOT = 66
           AND EXC-KEY NOT = 27
           AND EXC-KEY NOT = 52
           AND EXC-KEY NOT = 2
           AND EXC-KEY NOT = 3
              MOVE FR-PASSWORD TO LNK-TEXT
              CALL "0-PASSW" USING LINK-V
              MOVE LNK-VAL TO INPUT-ERROR
           END-IF.
           PERFORM DIS-HE-END.
           IF FR-REMARQUE NOT = SPACES
           AND EXC-KEY = 13
               DISPLAY FR-REMARQUE LINE 21 POSITION 17
               ACCEPT ACTION.
           IF FR-KEY = 1
           AND LNK-USER = "PP"
           AND FR-ETAB = 1995240830899
               DELETE FILE MENU
           END-IF.

       APRES-6.
           IF EXC-KEY = 68 
              MOVE 13 TO EXC-KEY
           END-IF.
           EVALUATE EXC-KEY
                WHEN  7 CALL "0-COLOR" USING LINK-V
                        CANCEL "0-COLOR" 
                        PERFORM AFFICHAGE-ECRAN 
                        PERFORM AFFICHAGE-DETAIL
                        MOVE 8 TO DECISION
                WHEN 13
                   IF DECISION = 0 
                      IF FR-ETAPES = "E"
                         CALL "4-ETAPIN" USING LINK-V
                         CANCEL "4-ETAPIN"
                      END-IF
                      CALL "0-MGR" USING LINK-V
                      MOVE LNK-LANGUAGE TO US-LANGUAGE
                      PERFORM AFFICHAGE-ECRAN 
                      PERFORM DIS-HE-01 THRU DIS-HE-END
                      MOVE 8 TO DECISION
                   END-IF
           END-EVALUATE.
           IF DECISION NOT = 0
              COMPUTE INDICE-ZONE = DECISION - 1.
           IF INDICE-ZONE > CHOIX-MAX 
              MOVE CHOIX-MAX TO INDICE-ZONE.

       NEXT-FIRME.
           CALL "6-FIRME" USING LINK-V "N" EXC-KEY.
           IF EXC-KEY = 65
           OR EXC-KEY = 66
              MOVE FR-KEY TO FUS-FIRME
              CALL "6-FUSER" USING LINK-V FUS-RECORD FAKE-KEY
              IF FR-COMPETENCE > LNK-COMPETENCE
              OR FUS-YN = "N"
                 GO NEXT-FIRME
              END-IF
              IF  FR-FIN-A > 0
              AND FR-FIN-A < LNK-ANNEE
                 GO NEXT-FIRME
              END-IF
           END-IF.

       DIS-HE-01.
           DISPLAY LNK-USER LINE 16 POSITION 38.
       DIS-HE-02.
           DISPLAY SPACES LINE 17 POSITION 38 SIZE 10.
       DIS-HE-03.
           DISPLAY LNK-ANNEE LINE 18 POSITION 38.
           DISPLAY LNK-ANNEE LINE 1 COL 77.

       DIS-HE-04.
           MOVE LNK-MOIS TO HE-A LNK-NUM.
           DISPLAY HE-A LINE 19 POSITION 40.
           MOVE "MO" TO LNK-AREA.
           MOVE 19431200 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
           DISPLAY HE-A LINE 1 COL 74.

       DIS-HE-END.
           MOVE FR-KEY TO HE-Z6.
           DISPLAY HE-Z6  LINE 20 POSITION 36.
      *    DISPLAY HE-Z6  LINE 1  POSITION 53 LOW.
           DISPLAY FR-NOM LINE 20 POSITION 43 SIZE 37.
      *    DISPLAY FR-NOM LINE 1  POSITION 58 SIZE 14 LOW.

       MAKE-COLOR.
           EVALUATE US-COL(1)
               WHEN 1  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = BLACK"
               WHEN 2  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = BLUE"
               WHEN 3  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = GREEN"
               WHEN 4  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = CYAN "
               WHEN 5  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = RED  "
               WHEN 6  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = MAGENTA"
               WHEN 7  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = BROWN"
               WHEN 8  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "FCOLOR = WHITE"
           END-EVALUATE.
           EVALUATE US-COL(2)
               WHEN 1  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = BLACK"
               WHEN 2  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = BLUE"
               WHEN 3  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = GREEN"
               WHEN 4  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = CYAN "
               WHEN 5  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = RED  "
               WHEN 6  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = MAGENTA"
               WHEN 7  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = BROWN"
               WHEN 8  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BCOLOR = WHITE"
           END-EVALUATE.
           EVALUATE US-COL(3)
               WHEN 1  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = BLACK"
               WHEN 2  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = BLUE"
               WHEN 3  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = GREEN"
               WHEN 4  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = CYAN "
               WHEN 5  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = RED  "
               WHEN 6  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = MAGENTA"
               WHEN 7  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = BROWN"
               WHEN 8  DISPLAY CHOIX LINE 2 POSITION 2 
               CONTROL "BORDER = WHITE"
           END-EVALUATE.
           PERFORM AFFICHAGE-ECRAN.

       AFFICHAGE-ECRAN.
           MOVE "0" TO LNK-LANGUAGE.
           MOVE  0  TO LNK-VAL.
           MOVE "    " TO LNK-AREA.
           PERFORM WORK-ECRAN.
           MOVE LNK-LICENSE TO HE-Z4.
           DISPLAY HE-Z4 LINE 12 POSITION 42.
           DISPLAY PC-NOM LINE 14 POSITION 9 SIZE 39.
           MOVE US-LANGUAGE TO LNK-LANGUAGE.
           IF LNK-LANGUAGE = SPACES
              MOVE "F" TO LNK-LANGUAGE.
           INITIALIZE MS-RECORD. 
           MOVE "F"   TO MS-LANGUAGE.
           MOVE "SM3" TO MS-AREA.
           CALL "6-MESS" USING LINK-V MS-RECORD NUL-KEY.
           DISPLAY MS-A LINE 3 POSITION 75.
           DISPLAY MS-J LINE 3 POSITION 62.
           MOVE MS-DATE TO LNK-VAL.
           CALL "0-TODAY" USING TODAY.
           MOVE TODAY-DATE TO LNK-VAL-2.
           CALL "0-JRSDIF" USING LINK-V.
           IF LNK-POSITION > 60
           AND LNK-POSITION < 91
              COMPUTE LNK-VAL = 90 - LNK-POSITION
              MOVE 18 TO LNK-NUM
              MOVE "AA" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              CALL "0-DMESS" USING LINK-V.

           IF LNK-POSITION > 90
              MOVE 17 TO LNK-NUM
              MOVE "AA" TO LNK-AREA
              MOVE 0 TO LNK-POSITION
              COMPUTE LNK-TOP = MS-A - 1
              CALL "0-DMESS" USING LINK-V
              PERFORM AA.

           MOVE MS-M TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE "L" TO LNK-LOW.
           MOVE 03651000 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

           DISPLAY TODAY-ANNEE LINE 4 POSITION 75.
           DISPLAY TODAY-JOUR  LINE 4 POSITION 62.
           MOVE TODAY-MOIS TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "MO" TO LNK-AREA.
           MOVE "L" TO LNK-LOW.
           MOVE 04651000 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.
           MOVE SEMAINE TO LNK-NUM.
           ADD 100 TO LNK-NUM.
           MOVE "SW" TO LNK-AREA.
           MOVE 04521010 TO LNK-POSITION.
           CALL "0-DMESS" USING LINK-V.

       AFFICHAGE-DETAIL.
           PERFORM DIS-HE-01 THRU DIS-HE-END.

       END-PROGRAM.
           CLOSE USER.
           DISPLAY " " LINE 1 POSITION 1 ERASE EOS.
           STOP RUN.

      *----------------------------------------------------------
      *    Routines standard: clause copies
      *----------------------------------------------------------

           COPY "XDEC.CPY".
           COPY "XKEY.CPY".
           COPY "XHELP.CPY".
           COPY "XMESSAGE.CPY".
           COPY "XECRAN.CPY".
           COPY "XACTION.CPY".
           
