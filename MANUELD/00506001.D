
            
            
            
            �berstunden in Ausgleichstunden umwandeln
            �����������������������������������������

            OBERGRENZE AUSGLEICHSTUNDEN X STUNDEN MAXIMAL / 0 = ohne Limit
            
            Die Gesamtreserve ist der aufgelaufene Stundenbestand
            der vorhergehenden Monate. Mit X kann man eine Obergrenze
            angeben die nicht �berschritten werden soll. Eventuell
            nicht umgewandelte Stunden werden als �berstunden aus-
            gezahlt.
            
            Um diese Grenze individuell festzulegen kann man im
            Programm "PERS" auf Seite 2 ein Limit eingeben:
            
            2 MAX BEZAHLTE STUNDEN

            das, wenn gr�sser 0, die Gesamtobergrenze ignoriert, aber
            das Monatslimit ber�cksichtigt wenn dieses > 0 ist.

            LIMIT DES MONATS Y STUNDEN MAXIMUM   / 0 = ohne Limit
            
            Maximalanzahl von �berstunden, die im laufenden Monat 
            umgewandelt werden d�rfen.
            
            Beispiele:
            
            Bestand = 20
            
            �berstunden = 20
            
            1)   X = 0    Y = 0
            Alle Stunden werden umgewandelt.
                
            2)   X = 25   Y = 0
            5 Stunden werden umgewandelt, der Bestand z�hlt 25.
             
            3)   X = 50   Y = 0
            Alle Stunden werden umgewandelt, die Obergrenze wurde
            nicht erreicht, der Bestand ist erst auf 40.
            
            4)   X = 40   Y = 10
            10 Stunden werden umgewandelt, der Bestand ist auf 30.
            
            5)   X = 0    Y = 10
            10 Stunden werden umgewandelt, der Bestand ist auf 30.
            
            ��������������������������������������������

            Um die Prozedur zu wiederholen:
            
            Falls man die Prozedur wiederholen m�chte um andere Resultate
            zu erzielen, mu� man zuerst die Lohnart (Standardlohnart = 60)
            l�schen. Im Programm "MOD" (Lohnart �ndern od. l�schen) kann man
            die Lohnart l�schen und im Programm "A" kann man den Lohn
            neuerstellen.
             
            ��������������������������������������������
            
            Programmierbemerkungen:

            Lohnart 60 steht in CP : 196
            Den �berstundenzuschlag vermeiden:
            Angabe 1 = 1 in der Predefinierung der Lohnart.


            