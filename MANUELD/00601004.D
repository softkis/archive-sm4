


                    MEHRFACHE KOSTENSTELLEN
                    컴컴컴컴컴컴컴컴컴컴컴�
                             

        A  Aufteilung
        
           Falls die L봦ne nach Kostenstellen 1 - 9999 verbucht werden,
           wird die in der Karriere erfasste Kostenstelle angewendet.
        
           Da manche Personen in mehreren Kostenstellen vertreten sind,
           kann man bis zu 10 Kostenstellen vorgeben. Die Aufteilung
           geschieht wie folgt:
           
                     Wert * Anteil der Kostenstelle
                     컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴
                     Gesamtzahl der definierten Anteile
           
           Beispiel: 
           
           Kostenstelle 1 = 1
           Kostenstelle 2 = 1
           Kostenstelle 3 = 2

           Das Resultat der Kostenstelle 3 ist
           
                     100 * 2
                     컴컴컴�  also 50 %
                        4     (= 1 + 1 + 2)

           Eine spezifische Quote kann 1 - 9999 betragen.
           
           Beispiel:
           
           Kostenstelle 1 = 2225 = 22,25%
           Kostenstelle 2 = 2225 = 22,25%
           Kostenstelle 3 = 5550 = 55,50%
    
        B  Aktualisierung
        
           St봲st das Programm w꼑rend der Aktualisierung der Journaldatei 
           auf einen spezifische Eintrag, wird die Hauptkostenstelle in 
           der Karriere ignoriert.
        
           
        C  Einschr꼗kungen
        
           Die verschiedenen Kostenstellen k봭nen in beliebiger Reihen-
           folge erfasst werden. Es muss allerdings mindestens eine
           Kostenstelle bestehen und es d걊fen keine L갷ken vorhanden sein 
           
           Der Minimalwert einer Kostenstelle Anteil = 1.
           
           