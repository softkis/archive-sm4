


                    STATUTEN DER KRANKENKASSEN
                    컴컴컴컴컴컴컴컴컴컴컴컴컴
                
        -   H섽E DES KRANKENGELDES
        
        Das Krankengeld wird bestimmt durch den Bruttoverdienst
        den der Betroffene bei normaler Arbeit verdient h꼝te.

        Daf걊 z꼑len die Stunden die der Lohnempf꼗ger normalerweise
        gearbeitet h꼝te. 
         
        Im Falle einer ungeregelten Arbeitszeit (Berufsfahrer), wird 
        das Krankengeld auf den Brutto-Durchschnitt der letzten 3 
        Monate vor der Arbeitsunf꼑igkeit gerechnet.
        
        Wenn der Lohn durch die H봦e der Einnahmen (Taxifahrer ) bestimmt
        wird, bezieht sich das Krankengeld auf den Brutto-Durchschnitt
        der letzten 12 Monaten vor der Arbeitsunf꼑igkeit.
         
        Wenn eine Person jedoch noch keine 3 beziehungsweise 12 Monate bei
        dem gleichen Arbeitgeber gearbeitet hat, dann werden nur die Monate, in
        denen die Person  bei diesem Arbeitgeber gearbeitet hat, in Betracht 
        gezogen.


        -               숥ERSTUNDEN
                
        Die 쉇erstunden werden nur ber갷ksichtigt, wenn in den
        letzten 3 Monaten vor der Arbeitsunf꼑igkeit regelm꾞ig 쉇erstunden 
        geleistet wurden und wenn die Person auch in der Krankheitsperiode 
        쉇erstunden geleistet h꼝te.
        
        Die 쉇erstunden m걌sen nicht (mehr) vom Arbeitsministerium 
        genehmigt worden sein um ber갷ksichtigt zu werden.
        
        Im Falle einer ungeregelten Arbeitszeit und falls die Person einen
        Monatslohn erh꼕t, hat diese Person recht auf eine Entsch꼋igung die
        sich auf den t꼏lichen Lohn bezieht. Dieser t꼏liche Lohn wird mit der
        Anzahl der arbeitsunf꼑igen Tage multipliziert.
         
        Das Krankengeld wird eventuell an den Index und an Lohnerh봦ungen 
        durch Tarifvertr꼏e angepasst.
        
        
        