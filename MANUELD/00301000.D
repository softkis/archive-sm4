


    
    
    Positionieren im Kalender:
            
        Oben   = Pfeil nach oben
        Unten  = Pfeil nach unten
        Links  = ESC    oder Ctrl + Pfeil nach links
        Rechts = Enter  oder Ctrl + Pfeil nach rechts
            
        Das Programm �berpr�ft die Ein- und Austrittsdaten
         
    F2  Das Programm kopiert die erfassten Stunden des vorhergehenden Tags.

    F3  Wenn man mit F3 = Anfang den ersten Tag kennzeichnet und mit F4 = 
        Ende den letzten, f�llt das Programm die so bestimmten Tage 
        einschliesslich mit dem Komplementarwert zur Sollarbeitszeit auf.

        (Man darf nicht vergessen, die unbezahlten Krankentage 
        = Samstage und Sonntage, negativ "-8" einzugeben. Mit F3=Anfang
        und F4=Ende geschieht dies automatisch.)

    F5  Stunden abspeichern und R�ckkehr auf das Feld 0
    
    F6  Das Programm f�llt alle Tage zwischen und mit den ersten und letzten 
        zul�ssigen Tagen des Monats einschliesslich mit dem Komplementarwert
        zur Sollarbeitszeit auf.
    
    F8  Einen Tag l�schen
    
    F9  Auf das Kommandofeld springen
    
        Seite -> Erfassen der Nachtstunden
    
        Von der Kommandozeile auf einen Tag setzen: Datum
       
        Eine T�tigkeit ganz l�schen: F8 auf der Kommandozeile oder auf 
        dem Feld 0
        
