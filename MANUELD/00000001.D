


            CHECKLISTE F�R DIE LOHNBERECHNUNG    

    ������������������������������������������������������������������

    1)  VORBEREITUNG 

    Neue Mitarbeiter mit "PERS" anlegen. 

    VOR der Lohnberechnung:
    Austrittsdatum eingeben in "CON" und Austrittserkl�rungen drucken !

    Karriere in "CAR" anpassen (Lohnerh�hung usw.)

    Steuerkarten in "FICHE" eingeben

    Steuern mit "RI" richtigstellen

    Mit PSOLDE Zahlungsunregelm�ssigkeiten �berpr�fen.


    2)  LOHNBERECHNUNG    

    Stunden in "HR" eingeben
    
    Individuallohnarten in "P" eingeben

    Krankengeldvorauszahlung eventuell in "MAL" unterbinden

    Krankenzettel mit "DM" drucken
    
    �berweisungen mit "V" vorbereiten

    �berweisungen mit "PV" drucken oder "VML" �berweisen 

    Lohnzettel mit "FP" drucken
    
    Fibu mit "O" drucken - Buchen = "J"


    Steuererkl�rung mit "DI" vorbereiten

    Steuererkl�rung mit "II" drucken

    Steuer�berweisung mit "VII" drucken oder mit "IML" �berweisen

    Seculine:        DECSAL
                     DECMAL
                     DECAFF
    
