

        Les �tapes de la carriere
        _________________________

        Page 1

         1 NUM�RO PERSONNE          
         2 NOM DE RECHERCHE         recherche alphab�tique avec F2 / F3

         3 ANN�E D'APPLICATION      d�finit ensemble avec le mois la date o�
         4 MOIS  D'APPLICATION      cette �tape devient effective

         5 CODE STATUT              d�finit le statut comme ouvrier, employ�
                                    fonctionnaire
                                    peut �tre utilis� pour la ventilation 
                                    de la pr�paration comptable

         6 R�GIME S�CURIT�          g�re le r�gime des cotisations sociales
           SOCIALE                  


         7 CODE CONTRAT COLLECTIF   soumis au contrat collectif du type
                                    indiqu�, la valeur par d�faut = 0

         8 CODE D�PARTEMENT         identique au centre de co�t
         9 CODE M�TIER              d�nomination officielle 

        10 SALAIRE                  0=HORAIRE 1=MENSUEL pay� � l'heure, au mois
        11 SALAIRE DE BASE ACTUEL   Salaire de base � l'indice actuel
                                    Le programme calcule et retient le salaire
                                    � l'indice 100 (-> contr�le page 3)
        
        12 HEURES JOUR              Heures de travail journali�res
        13 JOURS DE TRAVAIL SEMAINE Jours de travail normales en semaine
        14 HEURES MOYENNES MOIS     Le programme propose les heures moyennes
                                    mensuelles calcul�es sur les deux donn�es
                                    pr�cedentes. 
                                    La moyenne sert � d�finir les moyennes de 
                                    cong� le cas �ch�ant et le salaire horaire 
                                    de base pour heures suppl�mentaires des 
                                    personnes pay�es au mois.

        15 CODE CONG�:              Le code cong� d�finit la proc�dure de 
                                    r�mun�ration de cong� 
        ______________________________________________________________________

        Page 2


         1 VALEUR PRIME 1           Prime sp�cifique � d�finir individuellement
                                    en cas de besoin
         2 VALEUR PRIME 2           comme 1

         3 VALEUR PRIME 3           comme 1

         4 TARIF KILOM�TRIQUE  1-3  Code tarif de la r�mun�ration par kilom�tre
                                    La valeur relative est contenue dans le 
                                    fichier Contrat collectif.

         5 VALEUR D�PLACEMENT JOUR  Industrie du b�timent; g�re le payement du
                                    TEMPS de d�placement au chantier.
                                    1 = salaire horaire
                                    2 = valeur retenue dans le fichier contrat 
                                        collectif
                                    > 2 = valeur; la valeur du fichier contrat
                                    collectif est ignor�e

         6 CODE �QUIPE              �quipe de travail, peut d�finir les poses

         7 CODE DIVISION            regroupement compl�mentaire ou diff�rent 
                                    des d�partements pour les entreprises 
                                    int�ress�es par cette option

         8 CODE POSTE DE FRAIS      Poste de frais par d�faut pour les 
                                    entreprises travaillant avec cette option


         9 HORAIRE MOBILE           0 = non 1 = oui
                                    Si ce param�tre est mis, le programme ne
                                    paye plus des heures suppl�mentaires, mais
                                    g�re les temps diff�rents de l'horaire
                                    th�orique
                                    Sp�cial = 2
                                    Les heures suppl�mentaires sont trans-
                                    form�es en heures � r�cup�rer, le
                                    suppl�ment est pay�
                                    Sp�cial = 3
                                    Les heures suppl�mentaires et le
                                    suppl�ment sont transform�es en heures 
                                    � r�cup�rer
                                    

        10 HEURES JOUR F�RI� PR�FER.Pour chaque jour f�ri� le nombre d'heures
                                    retenu dans ce param�tre sera consid�r�
                                    avec priorit� absolue et m�me si la 
                                    journ�e avait �t� libre. 
                                    Convient surtout pour personnes avec
                                    horaires variables.

        11 TAUX  CONG� INT�RIMAIRE  Taux individuel prioritaire


        12 JOURS CONG� 0 = STANDARD Tout cong� diff�rent du minimum l�gal 
                                    retenu dans le fichier contrat collectif

        g�re la cr�ation des enqu�tes STATEC

        Page 3

         1 SALAIRE DE BASE I100     Salaire de base � l'indice 100

         2 COMPLEMENT SALAIRE       Si une ou plusieures primes d�finies
                                    par des codes salaires sp�cifiques
                                    font en principe partie du salaire de 
                                    base, on peut ici indiquer la valeur 
                                    cumul�e qui est alors ajout�e pour la 
                                    d�finition du salaire horaire moyen.
                                    La valeur est automatiquement adapt�e 
                                    � l'index.

         3 CODE STATEC              1 = APPRENTI
                                    2 = CONTREMAITRE
                                    Dans l'enqu�te semestrielle, un contre-
                                    ma�tre ou chef d'�quipe est repris parmi
                                    les employ�s
                                    3 = IGNOR�:
                                    g�rants,
                                    membres de la famille,
                                    personnel de nettoyage,
                                    handicap�s � salaire r�duit,
                                    travailleurs � domicile,

         4 CODE CHAMBRE DES METIERS Code groupe professionel, sert � �tablir
                                    l'enqu�te sur les gains des salari�s dans 
                                    l'Artisanat de la Chambre des M�tiers
                                    
         5 QUALIFICATION 1 - 4      idem / information compl�mentaire


         6 POSITION                 Description d�taill�e de la fonction
                                    exacte de la personne

         7 0=ACTIF    1=INACTIF     Si le code est 1=inactif, le programme ne
                                    permet pas l'acc�s jusqu'au moment o� le
                                    programme retrouve une �tape d'activit�.
                                    Convient pour les personnes en maladie
                                    prolong�e, cong� de maternit�, poste �
                                    l'�tranger ou autrement momentan�ment non
                                    disponible.
         8 CODE BAREME              1 lettre; d�finit le type de bar�me 
                                    auquel le salaire est li�
         9 GRADE BAREME SALAIRE     Si code bar�me est retenu le programme se
        10 �CHELON 1-40             pose sur ces zones, autrement elles sont
                                    ignor�es
                                    

        Page 4

        Primes suppl�mentaires : indiquer "N" si non inclus dans:
                                  Salaire de base      Gratification

        1 POINTS
        2 POINTS
        3 POINTS
        4 POINTS
        5 POINTS
        
           Fonctionnaires:

        16 POINTS ALLOCATION FAM    suppl�ment de salaire
        17 POINTS MAJORATION        suppl�ment de salaire

        Les points indiqu�s ici peuvent �tre exploit�s par des
        proc�dures (voir proc�dures) pour cr�er des suppl�ments
        de salaire.

        Si une ou plusieures primes d�finies font en principe partie 
        du salaire de base ou du 13e mois, on peut ici dire par "O" 
        ou "N" si la valeur est � ajouter pour la d�finition du salaire 
        horaire moyen, respectivement du 13e mois. (" " = oui)
        Les valeurs sont automatiquement adapt�es � l'index.
         
         